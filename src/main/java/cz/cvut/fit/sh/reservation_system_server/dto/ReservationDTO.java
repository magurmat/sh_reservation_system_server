package cz.cvut.fit.sh.reservation_system_server.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import lombok.Data;

import java.time.LocalDateTime;


@Data
public class ReservationDTO {

    private final int id;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private final LocalDateTime startTime;

    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private final LocalDateTime endTime;

    private final ReservationState state;

    private final int userId;
    private final String firstName;
    private final String lastName;

    private final int itemId;

}
