package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnsupportedImageFormatException;
import cz.cvut.fit.sh.reservation_system_server.facade.ItemFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin("*")
public class ItemController {

    private final ItemFacade itemFacade;

    public ItemController(ItemFacade itemFacade) {
        this.itemFacade = itemFacade;
    }

    @GetMapping("/item")
    ResponseEntity<List<ItemDTO>> allItems() throws UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findAll(), HttpStatus.OK);

    }

    @GetMapping("/user/{userId}/favourite")
    ResponseEntity<List<ItemDTO>> getUsersFavouriteItems(@PathVariable int userId) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findUsersFavourites(userId), HttpStatus.OK);

    }

    @GetMapping("/item/tag/{tagId}")
    ResponseEntity<List<ItemDTO>> getItemsByTag(@PathVariable int tagId) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findByTag(tagId), HttpStatus.OK);

    }

    @GetMapping("/location/{locationId}/item")
    ResponseEntity<List<ItemDTO>> getItemsByLocation(@PathVariable int locationId) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findByLocation(locationId), HttpStatus.OK);
    }

    @GetMapping("/item/{id}")
    ResponseEntity<ItemDTO> findItemById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findById(id), HttpStatus.OK);
    }

    @GetMapping("/item/")
    ResponseEntity<ItemDTO> findItemBySlug(@RequestParam String slug) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.findBySlug(slug), HttpStatus.OK);
    }


    @PostMapping(value = "/item", consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<ItemDTO> saveItem(@Valid @RequestPart("data") ItemCreateDTO item,
                     @RequestPart(name = "image", required = false)MultipartFile multipartFile) throws NotFoundByIdException, UnauthorizedException, CannotSaveImageException, UnsupportedImageFormatException {
        if(multipartFile != null) {
            item.setImage(multipartFile);
        }
        return new ResponseEntity<>(itemFacade.create(item), HttpStatus.OK);
    }

    @PostMapping("/item/{itemId}/tag/{tagId}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<ItemDTO> addTagToItem(@PathVariable int itemId, @PathVariable int tagId) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(itemFacade.addTag(itemId, tagId), HttpStatus.OK);
    }

    @PutMapping(value = "/item/{id}", consumes = "multipart/form-data")
    ResponseEntity<ItemDTO> updateItem(@PathVariable int id,
                                       @Valid @RequestPart("data") ItemUpdateDTO item,
                                               @RequestPart(name = "image", required = false)MultipartFile multipartFile)
            throws UnauthorizedException, NotFoundByIdException, CannotSaveImageException, UnsupportedImageFormatException {
        if(multipartFile != null) {
            item.setImage(multipartFile);
        }
        return new ResponseEntity<>(itemFacade.update(id, item), HttpStatus.OK);

    }



    @DeleteMapping("/item/{id}")
    ResponseEntity<?> deleteItem(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        itemFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
