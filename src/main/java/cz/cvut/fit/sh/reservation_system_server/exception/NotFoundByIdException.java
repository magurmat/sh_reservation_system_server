package cz.cvut.fit.sh.reservation_system_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundByIdException extends Exception{
    public NotFoundByIdException(int id, String entity)
    {
        super(entity + " with id " + id + " was not found!");
    }
}
