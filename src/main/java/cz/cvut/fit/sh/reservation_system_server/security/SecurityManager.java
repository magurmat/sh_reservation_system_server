package cz.cvut.fit.sh.reservation_system_server.security;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Component
public class SecurityManager {

    private final RoleService roleService;
    private static final Logger logger = LoggerFactory.getLogger(SecurityManager.class);

    @Autowired
    public SecurityManager(@Lazy RoleService roleService) {
        this.roleService = roleService;
    }

    public boolean getAllItemsAccess(Authentication auth){
        return true;
    }

    public boolean getItemAccess(int itemId, Authentication auth){
        return true;
    }

    public boolean createItemAccess(int locationId, List<Integer> tags, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        boolean managesTags = true;
        for(Integer tagId : tags){
            if(!managesTag(roles, tagId)){
                managesTags = false;
            }
        }
        return managesLocation(roles, locationId) && managesTags;
    }

    public boolean updateItemAccess(int itemId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesItem(roles, itemId);
    }

    public boolean deleteItemAccess(int itemId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesItem(roles, itemId);
    }

    public boolean addTagToItemAccess(int itemId, int tagId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesItem(roles, itemId) && managesTag(roles, tagId);
    }

    public boolean getAllLocationsAccess(Authentication auth){
        return true;
    }

    public boolean getLocationAccess(int locationId, Authentication auth){
        return true;
    }

    public boolean createLocationAccess(Integer parentLocationId, Authentication auth){
        if(auth == null){
            return false;
        }

        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        if(parentLocationId == null){
            return isAdmin(roles);
        }
        return managesLocation(roles, parentLocationId);
    }

    public boolean updateLocationAccessWithoutParentChange(int locationId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesLocation(roles, locationId);
    }

    public boolean updateLocationAccessWithParentChange(int locationId, Integer parentLocationId, Authentication auth){
        if(!createLocationAccess(parentLocationId, auth)){
            return false;
        }
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesLocation(roles, locationId);
    }

    public boolean deleteLocationAccess(int locationId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesLocation(roles, locationId);
    }

    public boolean getAllReservationsAccess(Authentication auth){
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        return isAdmin(roles);
    }

    public boolean getReservationAccess(Integer userId, Integer itemId, Integer locationId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        for(Role role : roles){
            if(managesLocation(role, locationId)){
                return true;
            }
            if(managesItem(role, itemId)){
                return true;
            }
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean createReservationAccess(Authentication auth){
        return true;
    }

    public boolean getAllReservationsByUserAccess(int userId, Authentication auth){
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean getAllReservationsByItemAccess(int itemId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        return managesItem(roles, itemId);
    }

    public boolean updateReservationAccess(int userId, int itemId, Authentication auth){
        if(auth == null){
            return false;
        }

        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();

        if(details.getUserId() == userId){
            return true;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesItem(roles, itemId);
    }

    public boolean updateReservationStateAccess(int userId, int itemId, ReservationState newState, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        for(Role role : roles){
            if(managesItem(role, itemId)){
                return true;
            }
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId && newState == ReservationState.CANCELLED;
    }

    public boolean deleteReservationAccess(Authentication auth){
        return false;
    }

    public boolean unconfirmedReservationsForUserAccess(int userId, Authentication auth){
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean upcomingReservationsForUserAccess(int userId, Authentication auth){
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean upcomingReservationsForItemAccess(int itemId, Authentication auth){
        if(auth == null){
            return false;
        }
        return true;
    }

    public boolean getAllRolesAccess(Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        return isAdmin(roles);
    }

    public boolean getRoleAccess(int roleId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        for(Role role : roles){
            if(role.getId() == roleId){
                return true;
            }
        }
        return managesRole(roles, roleId);
    }

    public boolean updateRoleAccess(int roleId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesRole(roles, roleId);
    }

    public boolean deleteRoleAccess(int roleId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesRole(roles, roleId);
    }

    //TODO change
    public boolean createRoleAccess(RoleCreateDTO roleCreateDTO, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        for(Integer tagId : roleCreateDTO.getManagedTagIds()){
            if(!managesTag(roles, tagId)){
                return false;
            }
        }
        for(Integer locationId : roleCreateDTO.getManagedLocationIds()){
            if(!managesLocation(roles, locationId)){
                return false;
            }
        }
        for(Integer roleId : roleCreateDTO.getManagedRoleIds()){
            if(!managesRole(roles, roleId)){
                return false;
            }
        }
        for(Integer itemId : roleCreateDTO.getManagedItemIds()){
            if(!managesItem(roles, itemId)){
                return false;
            }
        }
        if(roleCreateDTO.isAdmin()){
            return isAdmin(roles);
        }
        return roles.size() > 0;
    }

    public boolean getAllUsersAccess(Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        return isAdmin(roles);
    }

    public boolean banUserAccess(Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        return isAdmin(roles);
    }

    public boolean getUserAccess(int userId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        if(details.getUserId() == userId){
            return true;
        }
        return roles.size() > 0;
    }

    public boolean updateUserAccess(int userId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean deleteUserAccess(int userId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean addUsersFavouriteItemAccess(int userId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean removeUsersFavouriteItemAccess(int userId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        UserSecurityDetails details = (UserSecurityDetails) auth.getPrincipal();
        return details.getUserId() == userId;
    }

    public boolean getAllTagsAccess(Authentication auth){
        return true;
    }

    public boolean getTagAccess(int tagId, Authentication auth){
        return true;
    }

    public boolean updateTagAccess(int tagId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesTag(roles, tagId);
    }

    public boolean createTagAccess(Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return roles.size() > 0;
    }

    public boolean deleteTagAccess(int tagId, Authentication auth){
        if(auth == null){
            return false;
        }
        List<Role> roles = roleService.findByIds(loadRoles(auth.getAuthorities()));
        if(isAdmin(roles)){
            return true;
        }
        return managesTag(roles, tagId);
    }

    private List<Integer> loadRoles(Collection<? extends GrantedAuthority> authorities){
        List<Integer> roleIds = new ArrayList<>();
        for(GrantedAuthority authority : authorities){
            String roleId = authority.getAuthority().substring(5);
            roleIds.add(Integer.parseInt(roleId));
        }
        return roleIds;
    }

    private boolean managesItem(Role role, int itemId){
        for(Item managedItem : role.getManagedItems()){
            if(managedItem.getId() == itemId){
                return true;
            }
        }
        return false;
    }

    private boolean managesItem(List<Role> roles, int itemId){
        for(Role role : roles){
            if(managesItem(role, itemId)){
                return true;
            }
        }
        return false;
    }

    private boolean managesLocation(Role role, int locationId){
        for(Location managedLocation : role.getManagedLocations()){
            if(managedLocation.getId() == locationId){
                return true;
            }
        }
        return false;
    }

    private boolean managesLocation(List<Role> roles, int locationId){
        for(Role role : roles){
            if(managesLocation(role, locationId)){
                return true;
            }
        }
        return false;
    }

    private boolean managesRole(Role role, int roleId){
        for(Role managedRole : role.getManagedRoles()){
            if(managedRole.getId() == roleId){
                return true;
            }
        }
        return false;
    }

    private boolean managesRole(List<Role> roles, int roleId){
        for(Role role : roles){
            if(managesRole(role, roleId)){
                return true;
            }
        }
        return false;
    }

    private boolean managesTag(Role role, int tagId){
        for(Tag managedTag : role.getManagedTags()){
            if(managedTag.getId() == tagId){
                return true;
            }
        }
        return false;
    }

    private boolean managesTag(List<Role> roles, int tagId){
        for(Role role : roles){
            if(managesTag(role, tagId)){
                return true;
            }
        }
        return false;
    }

    private boolean isAdmin(List<Role> roles){
        for(Role role : roles){
            if(role.isAdmin())
                return true;
        }
        return false;
    }
}
