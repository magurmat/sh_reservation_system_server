package cz.cvut.fit.sh.reservation_system_server.controller;

import com.goebl.david.Webb;
import cz.cvut.fit.sh.reservation_system_server.dto.UserCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Ban;
import cz.cvut.fit.sh.reservation_system_server.entity.RefreshToken;
import cz.cvut.fit.sh.reservation_system_server.exception.EmailAlreadyUsedException;
import cz.cvut.fit.sh.reservation_system_server.exception.ExpiredRefreshTokenException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserAlreadyExistsException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserBannedException;
import cz.cvut.fit.sh.reservation_system_server.facade.UserFacade;
import cz.cvut.fit.sh.reservation_system_server.security.JwtUtil;
import cz.cvut.fit.sh.reservation_system_server.security.UserSecurityDetails;
import cz.cvut.fit.sh.reservation_system_server.security.UserSecurityDetailsService;
import cz.cvut.fit.sh.reservation_system_server.service.RefreshTokenService;
import cz.cvut.fit.sh.reservation_system_server.validation.PasswordMatches;
import cz.cvut.fit.sh.reservation_system_server.validation.ValidEmail;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Optional;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@CrossOrigin("*")
public class AuthenticateController {

    private final AuthenticationManager authenticationManager;
    private final UserSecurityDetailsService userSecurityDetailsService;
    private final UserFacade userFacade;
    private final PasswordEncoder passwordEncoder;
    private final RefreshTokenService refreshTokenService;

    @Autowired
    public AuthenticateController(AuthenticationManager authenticationManager,
                                  UserSecurityDetailsService userSecurityDetailsService, UserFacade userFacade,
                                  PasswordEncoder passwordEncoder, RefreshTokenService refreshTokenService) {
        this.authenticationManager = authenticationManager;
        this.userSecurityDetailsService = userSecurityDetailsService;
        this.userFacade = userFacade;
        this.passwordEncoder = passwordEncoder;
        this.refreshTokenService = refreshTokenService;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class AuthenticateRequest{
        private String username;
        private String password;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class RefreshTokenHolder{
        private String token;
    }

    @Getter
    @Setter
    @AllArgsConstructor
    @NoArgsConstructor
    public static class JWTResponse{
        private String jwt;
    }

    @PasswordMatches
    @Getter
    public static class UserRegistration {
        @NotNull
        @NotEmpty
        private String firstName;

        @NotNull
        @NotEmpty
        private String lastName;

        @NotNull
        @NotEmpty
        private String username;

        @NotNull
        @NotEmpty
        private String password;
        private String matchingPassword;

        @NotNull
        @NotEmpty
        @ValidEmail
        private String email;

    }

    @GetMapping("/oauth/is")
    public ResponseEntity<RefreshTokenHolder> oauthLogin(@RequestParam String code, @RequestParam String state) throws JSONException, NotFoundByIdException, UserAlreadyExistsException, EmailAlreadyUsedException, UserBannedException {
        String tokenExchange = "https://is.sh.cvut.cz/oauth/token";
        String userApi = "https://api.is.sh.cvut.cz/v1/users/me";
        Webb webb = Webb.create();

        JSONObject token = webb
                .post(tokenExchange)
                .param("code", code)
                .param("redirect_uri", System.getenv("ENV_CALLBACK_URI"))
                .param("client_id", System.getenv("ENV_CLIENT_ID"))
                .param("client_secret", System.getenv("ENV_CLIENT_SECRET"))
                .param("grant_type", "authorization_code")
                .asJsonObject()
                .getBody();

        webb.setDefaultHeader("Authorization", "Bearer " + token.getString("access_token"));

        JSONObject userData = webb
                .get(userApi)
                .asJsonObject()
                .getBody();

        UserSecurityDetails userDetails = userSecurityDetailsService.loadByEmail(userData.getString("email"));
        if(userDetails == null){
            UserCreateDTO userCreateDTO = new UserCreateDTO();
            userCreateDTO.setActive(true);
            userCreateDTO.setEmail(userData.getString("email"));
            userCreateDTO.setFirstName(userData.getString("first_name"));
            userCreateDTO.setUsername(userData.getString("username"));
            userCreateDTO.setLastName(userData.getString("surname"));
            userCreateDTO.setUid(userData.getInt("id"));
            userCreateDTO.setRoleIds(new ArrayList<>());
            userCreateDTO.setFavouriteIds(new ArrayList<>());

            userFacade.register(userCreateDTO);

            UserSecurityDetails userSecurityDetails = userSecurityDetailsService.loadUserByUsername(userCreateDTO.getUsername());
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userSecurityDetails.getUserId());
            return ok(new RefreshTokenHolder(refreshToken.getToken()));
        }
        else{
            Ban ban = userFacade.userBanned(userDetails.getUserId());
            if(ban != null){
                throw new UserBannedException(userDetails.getUsername(), ban.getExpirationDate());
            }
            RefreshToken refreshToken = refreshTokenService.createRefreshToken(userDetails.getUserId());
            return ok(new RefreshTokenHolder(refreshToken.getToken()));
        }

    }


    @PostMapping("/authenticate")
    public ResponseEntity<RefreshTokenHolder> auth(@RequestBody AuthenticateRequest authenticateRequest) throws NotFoundByIdException, UserBannedException {
        try {
            authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(authenticateRequest.getUsername(), authenticateRequest.getPassword()));
        } catch (BadCredentialsException e) {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, "Bad user credentials.");
        } catch (AuthenticationException e){
            throw new ResponseStatusException(HttpStatus.FORBIDDEN, e.getMessage());
        }
        UserSecurityDetails userSecurityDetails = userSecurityDetailsService.loadUserByUsername(authenticateRequest.getUsername());
        Ban ban = userFacade.userBanned(userSecurityDetails.getUserId());
        if(ban != null){
            throw new UserBannedException(userSecurityDetails.getUsername(), ban.getExpirationDate());
        }
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userSecurityDetails.getUserId());
        return ok(new RefreshTokenHolder(refreshToken.getToken()));
    }

    @PostMapping("/register")
    public ResponseEntity<RefreshTokenHolder> register(@Valid @RequestBody UserRegistration user) throws UserAlreadyExistsException, NotFoundByIdException, EmailAlreadyUsedException {
        UserCreateDTO userCreateDTO = new UserCreateDTO();
        userCreateDTO.setActive(true);
        userCreateDTO.setEmail(user.email);
        userCreateDTO.setFirstName(user.firstName);
        userCreateDTO.setUsername(user.username);
        userCreateDTO.setLastName(user.lastName);
        userCreateDTO.setPassword(passwordEncoder.encode(user.password));
        userCreateDTO.setUid(null);
        userCreateDTO.setRoleIds(new ArrayList<>());

        userFacade.register(userCreateDTO);

        UserSecurityDetails userSecurityDetails = userSecurityDetailsService.loadUserByUsername(user.getUsername());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(userSecurityDetails.getUserId());
        return ok(new RefreshTokenHolder(refreshToken.getToken()));
    }

    @PostMapping("/token")
    public ResponseEntity<JWTResponse> getJWT(@RequestParam String token) throws ExpiredRefreshTokenException, NotFoundByIdException, UserBannedException {
        Optional<RefreshToken> refreshToken = refreshTokenService.findByToken(token);
        if(refreshToken.isEmpty()){
            return ResponseEntity.notFound().build();
        }
        refreshTokenService.verifyExpiration(refreshToken.get());
        UserSecurityDetails userSecurityDetails = userSecurityDetailsService.loadUserByUsername(
                refreshToken.get().getUser().getUsername());
        Ban ban = userFacade.userBanned(userSecurityDetails.getUserId());
        if(ban != null){
            throw new UserBannedException(userSecurityDetails.getUsername(), ban.getExpirationDate());
        }
        String jwt = JwtUtil.generateToken(userSecurityDetails);
        return ok(new JWTResponse(jwt));
    }

}
