package cz.cvut.fit.sh.reservation_system_server.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "T_RESERVATION")
@NoArgsConstructor
@Getter
@Setter
@EqualsAndHashCode
@ToString
public class Reservation {

    public Reservation(LocalDateTime startTime,  LocalDateTime endTime, ReservationState state,
                        User user, Item item) {
        this.startTime = startTime;
        this.endTime = endTime;
        this.state = state;
        this.user = user;
        this.item = item;
    }

    @GeneratedValue
    @Id
    private int id;

    @Setter
    @Column(nullable = false)
    private LocalDateTime startTime;

    @Setter
    @Column(nullable = false)
    private LocalDateTime endTime;

    @Setter
    @Column(nullable = false)
    private ReservationState state;

    @ManyToOne(optional = false)
    @Setter
    private User user;

    @ManyToOne(optional = false)
    @Setter
    private Item item;

}
