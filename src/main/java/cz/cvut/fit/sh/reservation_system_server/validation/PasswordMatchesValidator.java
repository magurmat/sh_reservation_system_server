package cz.cvut.fit.sh.reservation_system_server.validation;

import cz.cvut.fit.sh.reservation_system_server.controller.AuthenticateController;
import cz.cvut.fit.sh.reservation_system_server.validation.PasswordMatches;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PasswordMatchesValidator
        implements ConstraintValidator<PasswordMatches, Object> {

    @Override
    public void initialize(PasswordMatches constraintAnnotation) {
    }
    @Override
    public boolean isValid(Object obj, ConstraintValidatorContext context){
        AuthenticateController.UserRegistration user = (AuthenticateController.UserRegistration) obj;
        return user.getPassword().equals(user.getMatchingPassword());
    }
}