package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.UserCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class UserService {

    private final UserRepository userRepository;
    private static final Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public List<User> findByIds(List<Integer> ids) {
        return userRepository.findAllById(ids);
    }

    public List<User> findByRole(Role role) {
        return userRepository.findAllByRolesIn(Set.of(role));
    }

    public Optional<User> findById(int id) {
        return userRepository.findById(id);
    }

    public Optional<User> findByUsername(String username) {
        return userRepository.findUserByUsername(username);
    }

    public Optional<User> findByEmail(String email) {
        return userRepository.findUserByEmail(email);
    }

    public User create(UserCreateDTO userCreateDTO, List<Role> roles, List<Item> favouriteItems){
        logger.info("Creating new user.");

        return userRepository.save(
                new User(
                        userCreateDTO.getFirstName(),
                        userCreateDTO.getLastName(),
                        userCreateDTO.getEmail(),
                        userCreateDTO.getPassword(),
                        userCreateDTO.getUsername(),
                        userCreateDTO.getUid(),
                        roles,
                        userCreateDTO.isActive(),
                        favouriteItems
                )
        );
    }

    public User update(int id, UserUpdateDTO userDTO, List<Role> roles, List<Item> favouriteItems) throws NotFoundByIdException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(id, "User");

        logger.info("Updating user with ID: {}.", id);

        User user = optionalUser.get();
        user.setFirstName(userDTO.getFirstName());
        user.setLastName(userDTO.getLastName());
        user.setEmail(userDTO.getEmail());
        user.setUsername(userDTO.getUsername());
        user.setUid(userDTO.getUid());
        user.setRoles(roles);
        user.setFavourites(favouriteItems);
        user.setActive(userDTO.isActive());
        userRepository.save(user);
        return user;
    }

    public void deleteById(int id) throws NotFoundByIdException {
        Optional<User> optionalUser = userRepository.findById(id);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(id, "User");

        logger.info("Deleting user with ID: {}.", id);

        userRepository.deleteById(id);
    }

    @Transactional
    public void removeItem(Item item){
        List<User> users = userRepository.findAllByFavouritesIn(List.of(item));
        for(User user : users){
            user.getFavourites().remove(item);
        }
    }

    public void removeRoleFromUser(User user, Role role){
        user.getRoles().remove(role);
    }

}
