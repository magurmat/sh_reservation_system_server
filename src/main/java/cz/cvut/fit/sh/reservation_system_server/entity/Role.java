package cz.cvut.fit.sh.reservation_system_server.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "T_ROLE")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Role {

    public Role(List<Item> managedItems, List<Role> managedRoles, List<Location> managedLocations,
                List<Tag> managedTags, String name, boolean isAdmin) {
        this.managedItems = managedItems;
        this.managedRoles = managedRoles;
        this.managedLocations = managedLocations;
        this.managedTags = managedTags;
        this.isAdmin = isAdmin;
        this.name = name;
    }

    @GeneratedValue
    @Id
    private int id;

    @Setter
    @Column(nullable = false, unique = true)
    private String name;

    @ManyToMany
    @Setter
    private List<Item> managedItems;

    @ManyToMany
    @Setter
    private List<Role> managedRoles;

    @ManyToMany
    @Setter
    private List<Location> managedLocations;

    @ManyToMany
    @Setter
    private List<Tag> managedTags;

    @Setter
    @Column(nullable = false)
    private boolean isAdmin;
}
