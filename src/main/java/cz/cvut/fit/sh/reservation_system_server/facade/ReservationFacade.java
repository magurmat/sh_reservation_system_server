package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ReservationDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ReservationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemDisabledException;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemOccupiedException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Set;


@Component
public class ReservationFacade {
    private final ReservationService reservationService;
    private final ItemService itemService;
    private final UserService userService;
    private final RoleService roleService;
    private final SecurityManager securityManager;

    @Autowired
    public ReservationFacade(ReservationService reservationService, ItemService itemService, UserService userService, RoleService roleService, SecurityManager securityManager) {
        this.reservationService = reservationService;
        this.itemService = itemService;
        this.userService = userService;
        this.roleService = roleService;
        this.securityManager = securityManager;
    }

    public List<ReservationDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllReservationsAccess(SecurityContextHolder.getContext().getAuthentication())){
            return reservationService.findAll().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to reservations!");
    }

    public List<ReservationDTO> findByIds(List<Integer> ids) throws NotFoundByIdException, UnauthorizedException {
        for(Integer reservationId : ids){
            Reservation reservation = reservationService.findById(reservationId).orElseThrow(() ->new NotFoundByIdException(reservationId,"reservation"));
            if(!securityManager.getReservationAccess(reservation.getUser().getId(), reservation.getItem().getId(),
                    reservation.getItem().getLocation().getId(), SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(reservationId,"reservation");
            }
        }
        return reservationService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public ReservationDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        Reservation reservation = reservationService.findById(id).orElseThrow(() ->new NotFoundByIdException(id,"reservation"));
        if(securityManager.getReservationAccess(reservation.getUser().getId(), reservation.getItem().getId(),
                reservation.getItem().getLocation().getId(), SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(reservation);
        }
        throw new UnauthorizedException(id,"reservation");
    }

    public List<ReservationDTO> findByUserId(int id) throws UnauthorizedException {
        if(securityManager.getAllReservationsByUserAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return reservationService.findByUserId(id).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to users reservations!");
    }

    public List<ReservationDTO> findByItemId(int id) throws UnauthorizedException {
        if(securityManager.getAllReservationsByItemAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return reservationService.findByItemId(id).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to items reservations!");

    }

    public ReservationDTO update(int id, ReservationUpdateDTO reservationCreateDTO) throws NotFoundByIdException, UnauthorizedException {
        Optional<Reservation> optionalReservation = reservationService.findById(id);
        if (optionalReservation.isEmpty())
            throw new NotFoundByIdException(id, "Reservation");

        if(!securityManager.updateReservationAccess(optionalReservation.get().getUser().getId(),
                optionalReservation.get().getItem().getId(),
                SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"reservation");
        }

        Item item;

        Optional<Item> optionalItem = itemService.findById(reservationCreateDTO.getItemId());
        if(optionalItem.isEmpty())
            throw new NotFoundByIdException(reservationCreateDTO.getItemId(), "Item");
        item = optionalItem.get();


        User user;

        Optional<User> optionalUser = userService.findById(reservationCreateDTO.getUserId());
        if(optionalUser.isEmpty())
            throw new NotFoundByIdException(reservationCreateDTO.getUserId(), "User");
        user = optionalUser.get();

        return toDTO(reservationService.update(id, reservationCreateDTO, user, item));
    }

    public ReservationDTO updateState(int id, String state) throws NotFoundByIdException, UnauthorizedException {
        Optional<Reservation> optionalReservation = reservationService.findById(id);
        if (optionalReservation.isEmpty())
            throw new NotFoundByIdException(id, "Reservation");
        Reservation reservation = optionalReservation.get();
        ReservationState newState = ReservationState.valueOf(state.toUpperCase(Locale.ROOT));

        if(!securityManager.updateReservationStateAccess(reservation.getUser().getId(), reservation.getItem().getId(),
                newState, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"reservation");
        }

        ReservationUpdateDTO reservationDTO = new ReservationUpdateDTO(reservation.getStartTime(),
                reservation.getEndTime(),
                newState,
                reservation.getUser().getId(),
                reservation.getItem().getId());
        return toDTO(reservationService.update(id, reservationDTO, reservation.getUser(), reservation.getItem()));
    }

    public ReservationDTO create(ReservationCreateDTO reservationCreateDTO) throws UnauthorizedException, NotFoundByIdException, ItemOccupiedException, ItemDisabledException {
        if(!securityManager.createReservationAccess(SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException("reservation");
        }
        Item item;
        Optional<Item> optionalItem = itemService.findById(reservationCreateDTO.getItemId());
        if(optionalItem.isEmpty())
            throw new NotFoundByIdException(reservationCreateDTO.getItemId(), "Item");
        item = optionalItem.get();

        if(itemAlreadyOccupied(reservationCreateDTO.getStartTime(), reservationCreateDTO.getEndTime(), item)){
            throw new ItemOccupiedException();
        }

        if(!item.getEnabled()){
            throw new ItemDisabledException();
        }
        User user;
        Optional<User> optionalUser = userService.findById(reservationCreateDTO.getUserId());
        if(optionalUser.isEmpty())
            throw new NotFoundByIdException(reservationCreateDTO.getUserId(), "User");
        user = optionalUser.get();


        return toDTO(reservationService.create(reservationCreateDTO, user, item));
    }

    //DELETE ON RESERVATION IS FORBIDDEN
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<Reservation> optionalReservation = reservationService.findById(id);
        if(optionalReservation.isEmpty())
            throw new NotFoundByIdException(id, "Reservation");

        if(!securityManager.deleteReservationAccess(SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id, "reservation");
        }
        reservationService.deleteById(id);
    }

    public Set<ReservationDTO> findUnconfirmedReservationsForUser(int userID) throws NotFoundByIdException, UnauthorizedException {
        Set<ReservationDTO> result = new HashSet<>();

        Optional<User> optionalUser = userService.findById(userID);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(userID, "User");

        if(!securityManager.unconfirmedReservationsForUserAccess(userID, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(userID, "user");
        }
        User user = optionalUser.get();
        for(Role role : user.getRoles()){
            result.addAll(findUnconfirmedReservationsForRole(role.getId()));
        }
        return result;
    }

    private Set<ReservationDTO> findUnconfirmedReservationsForRole(int roleID) throws NotFoundByIdException {
        Set<ReservationDTO> result = new HashSet<>();

        Optional<Role> optionalRole = roleService.findById(roleID);
        if (optionalRole.isEmpty())
            throw new NotFoundByIdException(roleID, "Role");

        Role role = optionalRole.get();
        for(Item item : role.getManagedItems()){
            List<Reservation> reservations = reservationService.findByItemId(item.getId());
            for(Reservation reservation : reservations){
                if(reservation.getState() == ReservationState.CREATED){
                    result.add(toDTO(reservation));
                }
            }
        }
        return result;
    }

    public Set<ReservationDTO> findUpcomingReservationsForUser(int userID) throws NotFoundByIdException, UnauthorizedException {
        Set<ReservationDTO> result = new HashSet<>();
        Optional<User> optionalUser = userService.findById(userID);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(userID, "User");
        if(!securityManager.upcomingReservationsForUserAccess(userID, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(userID, "user");
        }

        List<Reservation> reservations = reservationService.findByUserId(userID);
        for(Reservation reservation : reservations){
            if(reservation.getState() != ReservationState.CONFIRMED && reservation.getState() != ReservationState.CREATED){
                continue;
            }
            if(reservation.getStartTime().isAfter(LocalDateTime.now()) ||
                    (reservation.getStartTime().isBefore(LocalDateTime.now()) && reservation.getEndTime().isAfter(LocalDateTime.now()))){
                result.add(toDTO(reservation));
            }
        }
        return result;
    }

    public Set<ReservationDTO> findUpcomingReservationsForItem(int itemID) throws NotFoundByIdException, UnauthorizedException {
        Set<ReservationDTO> result = new HashSet<>();

        Optional<Item> optionalItem = itemService.findById(itemID);
        if (optionalItem.isEmpty())
            throw new NotFoundByIdException(itemID, "Item");

        if(!securityManager.upcomingReservationsForItemAccess(itemID, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(itemID, "item");
        }

        List<Reservation> reservations = reservationService.findByItemId(itemID);

        for(Reservation reservation : reservations){
            if(reservation.getState() != ReservationState.CONFIRMED && reservation.getState() != ReservationState.CREATED){
                continue;
            }
            if(reservation.getStartTime().isAfter(LocalDateTime.now()) ||
                    (reservation.getStartTime().isBefore(LocalDateTime.now()) && reservation.getEndTime().isAfter(LocalDateTime.now()))){
                result.add(toDTO(reservation));
            }
        }
        return result;
    }

    private boolean itemAlreadyOccupied(LocalDateTime startTime, LocalDateTime endTime, Item item){
        List<Reservation> allReservations = reservationService.findByItemId(item.getId());
        for(Reservation reservation : allReservations){
            if((startTime.isAfter(reservation.getStartTime()) && startTime.isBefore(reservation.getEndTime())) ||
                    (endTime.isAfter(reservation.getStartTime()) && endTime.isBefore(reservation.getEndTime())) ||
                    (startTime.isBefore(reservation.getStartTime()) && endTime.isAfter(reservation.getEndTime())) ||
                    (startTime.equals(reservation.getStartTime())) || (endTime.equals(reservation.getEndTime()))){
                if(reservation.getState() == ReservationState.CREATED || reservation.getState() == ReservationState.CONFIRMED){
                    return true;
                }
            }
        }
        return false;
    }

    private ReservationDTO toDTO(Reservation reservation){
        return new ReservationDTO(reservation.getId(), reservation.getStartTime(), reservation.getEndTime(),
                reservation.getState(), reservation.getUser().getId(), reservation.getUser().getFirstName(),
                reservation.getUser().getLastName(), reservation.getItem().getId());
    }

    private Optional<ReservationDTO> toDTO(Optional<Reservation> reservation){
        if(reservation.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(reservation.get()));
    }
}
