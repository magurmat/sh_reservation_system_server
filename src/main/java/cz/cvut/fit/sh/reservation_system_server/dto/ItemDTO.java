package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

import java.util.List;

@Data
public class ItemDTO {

    private final int id;

    private final String description;

    private final String name;

    private final Integer locationId;

    private final String slug;

    private final List<Integer> tagIds;

    private final Integer imageId;

    private final Boolean enabled;
}
