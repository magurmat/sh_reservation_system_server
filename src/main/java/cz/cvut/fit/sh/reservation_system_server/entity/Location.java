package cz.cvut.fit.sh.reservation_system_server.entity;

import cz.cvut.fit.sh.reservation_system_server.validation.KebabCase;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name = "T_LOCATION")
@NoArgsConstructor
@Getter
public class Location {

    public Location(Location parentLocation, String name, String description, String slug, Image image) {
        this.parentLocation = parentLocation;
        this.name = name;
        this.description = description;
        this.slug = slug;
        this.image = image;
    }

    @GeneratedValue
    @Id
    private int id;

    @ManyToOne(optional = true)
    @Setter
    private Location parentLocation;

    @Column(nullable = false)
    @Setter
    private String name;

    @Setter
    private String description;

    @Column(nullable = false, unique = true)
    @Setter
    @KebabCase
    private String slug;

    @OneToOne
    @Setter
    private Image image;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return id == location.id && name.equals(location.name) && Objects.equals(description, location.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, description);
    }
}
