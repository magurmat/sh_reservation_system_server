package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.BanCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Ban;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.BanRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class BanService {

    private final BanRepository banRepository;
    private static final Logger logger = LoggerFactory.getLogger(BanService.class);

    @Autowired
    public BanService(BanRepository banRepository) {
        this.banRepository = banRepository;
    }

    public List<Ban> findAll() {
        return banRepository.findAll();
    }

    public List<Ban> findByIds(List<Integer> ids) {
        return banRepository.findAllById(ids);
    }

    public Optional<Ban> findById(int id) {
        return banRepository.findById(id);
    }

    public Ban create(BanCreateDTO banCreateDTO, User user){
        logger.info("Creating new ban.");
        return banRepository.save(
                new Ban(
                        user,
                        banCreateDTO.getExpirationDate()
                )
        );
    }

    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Ban> optionalBan = banRepository.findById(id);
        if(optionalBan.isEmpty()) {
            throw new NotFoundByIdException(id, "Ban");
        }

        logger.info("Deleting ban with ID: {}.", id);

        banRepository.deleteById(id);
    }

    public List<Ban> findByUser(User user){
        return banRepository.findAllByUser(user);
    }
}
