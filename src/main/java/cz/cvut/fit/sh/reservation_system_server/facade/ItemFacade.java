package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnsupportedImageFormatException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class ItemFacade {

    private final ItemService itemService;
    private final ReservationService reservationService;
    private final LocationService locationService;
    private final RoleService roleService;
    private final UserService userService;
    private final TagService tagService;
    private final ImageService imageService;
    private final SecurityManager securityManager;

    @Autowired
    public ItemFacade(ItemService itemService, ReservationService reservationService, LocationService locationService,
                      RoleService roleService, UserService userService, TagService tagService, ImageService imageService,
                      SecurityManager securityManager) {
        this.itemService = itemService;
        this.reservationService = reservationService;
        this.locationService = locationService;
        this.roleService = roleService;
        this.userService = userService;
        this.tagService = tagService;
        this.imageService = imageService;
        this.securityManager = securityManager;
    }

    public List<ItemDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllItemsAccess(SecurityContextHolder.getContext().getAuthentication())){
            return itemService.findAll().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to items!");
    }

    //TODO change security check
    public List<ItemDTO> findUsersFavourites(int userId) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getAllItemsAccess(SecurityContextHolder.getContext().getAuthentication())){
            User user = userService.findById(userId).orElseThrow(() -> new NotFoundByIdException(userId, "user"));
            return user.getFavourites().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to items!");

    }

    public List<ItemDTO> findByLocation(int locationId) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getAllItemsAccess(SecurityContextHolder.getContext().getAuthentication())){
            Location location = locationService.findById(locationId).orElseThrow(() -> new NotFoundByIdException(locationId, "location"));
            return itemService.findByLocation(location).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to items!");

    }

    public List<ItemDTO> findByIds(List<Integer> ids) throws UnauthorizedException{
        for(Integer itemId : ids){
            if(!securityManager.getItemAccess(itemId, SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(itemId,"item");
            }
        }
        return itemService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public List<ItemDTO> findByTag(int tagId) throws UnauthorizedException, NotFoundByIdException {
        if(securityManager.getAllItemsAccess(SecurityContextHolder.getContext().getAuthentication())){
            Tag tag = tagService.findById(tagId).orElseThrow(() -> new NotFoundByIdException(tagId, "tag"));
            return itemService.findByTag(tag).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to items!");
    }

    //TODO better exception
    public ItemDTO findBySlug(String slug) throws NotFoundByIdException, UnauthorizedException {
        Item item = itemService.findBySlug(slug).orElseThrow(() -> new NotFoundByIdException(0,"Item"));
        if(securityManager.getItemAccess(item.getId(), SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(item);
        }
        throw new UnauthorizedException(slug,"item");
    }

    public ItemDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getItemAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(itemService.findById(id)).orElseThrow(() ->new NotFoundByIdException(id,"Item"));
        }
        throw new UnauthorizedException(id,"item");
    }

    @Transactional
    public ItemDTO addTag(int itemId, int tagId) throws NotFoundByIdException, UnauthorizedException {
        Item item = itemService.findById(itemId).orElseThrow(() ->new NotFoundByIdException(itemId,"Item"));
        Tag tag = tagService.findById(tagId).orElseThrow(() ->new NotFoundByIdException(tagId,"Tag"));
        if(!securityManager.addTagToItemAccess(itemId, tagId, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException();
        }
        if(!item.getTags().contains(tag)){
            item.getTags().add(tag);
        }
        return toDTO(item);
    }

    //TODO auth tags
    public ItemDTO create(ItemCreateDTO itemCreateDTO) throws NotFoundByIdException, UnauthorizedException, CannotSaveImageException, UnsupportedImageFormatException {
        Optional<Location> optionalLocation = locationService.findById(itemCreateDTO.getLocationId());
        if(optionalLocation.isEmpty()) {
            throw new NotFoundByIdException(itemCreateDTO.getLocationId(), "Location");
        }

        if(securityManager.createItemAccess(itemCreateDTO.getLocationId(), itemCreateDTO.getTagIds(),
                SecurityContextHolder.getContext().getAuthentication())){
            List<Tag> tags;
            if(itemCreateDTO.getTagIds() == null){
                tags = new ArrayList<>();
            }
            else {
                tags = tagService.findByIds(itemCreateDTO.getTagIds());
            }
            Image image = null;
            if(itemCreateDTO.getImage() != null) {
                image = imageService.create(itemCreateDTO.getImage());
            }
            Item item = itemService.create(itemCreateDTO, optionalLocation.get(), tags, image);
            roleService.addItemToTags(item, tags);
            roleService.addItemToLocation(item, optionalLocation.get());
            return toDTO(item);
        }
        throw new UnauthorizedException(itemCreateDTO.getLocationId(),"location");

    }

    public ItemDTO update(int id, ItemUpdateDTO itemDTO) throws NotFoundByIdException, UnauthorizedException, CannotSaveImageException, UnsupportedImageFormatException {
        Optional<Item> optionalItem = itemService.findById(id);
        if (optionalItem.isEmpty())
            throw new NotFoundByIdException(id, "Item");

        Optional<Location> optionalLocation = locationService.findById(itemDTO.getLocationId());
        if(optionalLocation.isEmpty()) {
            throw new NotFoundByIdException(itemDTO.getLocationId(), "Location");
        }
        List<Tag> tags = tagService.findByIds(itemDTO.getTagIds());
        if(securityManager.updateItemAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            Image image = null;
            if(itemDTO.getImage() != null) {
                image = imageService.create(itemDTO.getImage());
                if(optionalItem.get().getImage() != null){
                    imageService.deleteImage(optionalItem.get().getImage());
                }
            }
            else if (optionalItem.get().getImage() != null){
                image = optionalItem.get().getImage();
            }
            Item item = itemService.update(id, itemDTO, optionalLocation.get(), tags, image);
            roleService.addItemToTags(item, tags);
            roleService.addItemToLocation(item, optionalLocation.get());
            return toDTO(item);
        }
        throw new UnauthorizedException(id,"item");

    }

    @Transactional
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<Item> optionalItem = itemService.findById(id);
        if(optionalItem.isEmpty()) {
            throw new NotFoundByIdException(id, "Item");
        }

        if(securityManager.deleteItemAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            List<Reservation> reservations = reservationService.findByItemId(id);
            for(Reservation reservation : reservations){
                reservationService.deleteById(reservation.getId());
            }
            roleService.removeManagedItem(optionalItem.get());
            userService.removeItem(optionalItem.get());
            itemService.deleteById(id);
        }
        throw new UnauthorizedException(id,"item");

    }


    private ItemDTO toDTO(Item item) {
        Integer imageId = null;
        if(item.getImage() != null){
            imageId = item.getImage().getId();
        }
        return new ItemDTO(item.getId(), item.getDescription(), item.getName(),item.getLocation().getId(),
                item.getSlug(), item.getTags().stream().map(Tag::getId).toList(), imageId, item.getEnabled());
    }

    private Optional<ItemDTO> toDTO(Optional<Item> item) {
        if (item.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(item.get()));
    }
}
