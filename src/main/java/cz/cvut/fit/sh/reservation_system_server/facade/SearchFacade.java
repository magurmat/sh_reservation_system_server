package cz.cvut.fit.sh.reservation_system_server.facade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class SearchFacade {

    private final ItemFacade itemFacade;
    private final LocationFacade locationFacade;
    private final TagFacade tagFacade;

    private enum EntityType{
        ITEM,
        LOCATION,
        TAG
    }

    @Autowired
    public SearchFacade(ItemFacade itemFacade, LocationFacade locationFacade, TagFacade tagFacade) {
        this.itemFacade = itemFacade;
        this.locationFacade = locationFacade;
        this.tagFacade = tagFacade;
    }

    public List<String> all(){
        List<String> result = new ArrayList<>();

        return result;

    }




}
