package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

@Data
public class LocationDTO {

    private final int id;

    private final Integer parentLocationId;

    private final String name;

    private final String description;

    private final String slug;

    private final Integer imageId;
}
