package cz.cvut.fit.sh.reservation_system_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(HttpStatus.BAD_REQUEST)
public class EmailAlreadyUsedException extends Exception{
    public EmailAlreadyUsedException ()
    {
        super("Mail is already used by other user!");
    }
}
