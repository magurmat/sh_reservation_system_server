package cz.cvut.fit.sh.reservation_system_server.exception;

import java.time.LocalDateTime;

public class UserBannedException extends Exception{
    public UserBannedException(String username, LocalDateTime expirationDate)
    {
        super("User with username " + username + " is banned until + " + expirationDate.toString() + "!");
    }
}
