package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.RoleUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.RoleRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class RoleService {

    private final RoleRepository roleRepository;
    private static final Logger logger = LoggerFactory.getLogger(RoleService.class);

    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public List<Role> findAll() {
        return roleRepository
                .findAll();
    }

    @Transactional
    public Role update(int id, RoleUpdateDTO roleUpdateDTO, List<Role> roles, List<Location> locations,
                       List<Item> items, List<Tag> tags) throws NotFoundByIdException {
        Optional<Role> optionalRole = findById(id);
        if (optionalRole.isEmpty())
            throw new NotFoundByIdException(id, "Role");

        logger.info("Updating role with ID: {}.", id);

        Role role = optionalRole.get();
        role.setAdmin(roleUpdateDTO.isAdmin());
        role.setManagedItems(items);
        role.setManagedLocations(locations);
        role.setManagedRoles(roles);
        role.setManagedTags(tags);
        role.setName(roleUpdateDTO.getName());
        return role;
    }

    public Role create(RoleCreateDTO roleCreateDTO, List<Role> roles, List<Location> locations,
                          List<Item> items, List<Tag> tags){
        logger.info("Creating new role.");

        Role role = new Role(items,
                roles,
                locations,
                tags,
                roleCreateDTO.getName(),
                roleCreateDTO.isAdmin());
        return roleRepository.save(role);
    }


    public List<Role> findByIds(List<Integer> ids) {
        return roleRepository.findAllById(ids);
    }

    public Optional<Role> findById(int id) {
        return roleRepository.findById(id);
    }

    @Transactional
    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Role> optionalRole = roleRepository.findById(id);
        if(optionalRole.isEmpty())
            throw new NotFoundByIdException(id, "Role");

        logger.info("Deleting role with ID: {}.", id);

        removeManagedRole(optionalRole.get());
        roleRepository.deleteById(id);
    }

    public void removeManagedLocation(Location location) throws NotFoundByIdException {
        List<Role> roles = roleRepository.findAllByManagedLocationsIn(List.of(location));
        for(Role role : roles){
            List<Location> locations = new ArrayList<>(role.getManagedLocations());
            locations.remove(location);
            RoleUpdateDTO roleUpdateDTO = new RoleUpdateDTO(role.getName(),
                    role.getManagedItems().stream().map(Item::getId).toList(),
                    role.getManagedRoles().stream().map(Role::getId).toList(),
                    locations.stream().map(Location::getId).toList(),
                    role.getManagedTags().stream().map(Tag::getId).toList(),
                    role.isAdmin());
            update(role.getId(), roleUpdateDTO, role.getManagedRoles(), locations, role.getManagedItems(),
                    role.getManagedTags());
        }
    }

    public void removeManagedItem(Item item) throws NotFoundByIdException {
        List<Role> roles = roleRepository.findAllByManagedItemsIn(List.of(item));
        for(Role role : roles){
            List<Item> items = new ArrayList<>(role.getManagedItems());
            items.remove(item);
            RoleUpdateDTO roleUpdateDTO = new RoleUpdateDTO(role.getName(),
                    items.stream().map(Item::getId).toList(),
                    role.getManagedRoles().stream().map(Role::getId).toList(),
                    role.getManagedLocations().stream().map(Location::getId).toList(),
                    role.getManagedTags().stream().map(Tag::getId).toList(),
                    role.isAdmin());
            update(role.getId(), roleUpdateDTO, role.getManagedRoles(),role.getManagedLocations(), items,
                    role.getManagedTags());
        }
    }

    public void removeManagedTag(Tag tag) throws NotFoundByIdException {
        List<Role> roles = roleRepository.findAllByManagedTagsIn(List.of(tag));
        for(Role role : roles){
            List<Tag> tags = new ArrayList<>(role.getManagedTags());
            tags.remove(tag);
            RoleUpdateDTO roleUpdateDTO = new RoleUpdateDTO(role.getName(),
                    role.getManagedItems().stream().map(Item::getId).toList(),
                    role.getManagedRoles().stream().map(Role::getId).toList(),
                    role.getManagedLocations().stream().map(Location::getId).toList(),
                    tags.stream().map(Tag::getId).toList(),
                    role.isAdmin());
            update(role.getId(), roleUpdateDTO, role.getManagedRoles(),role.getManagedLocations(), role.getManagedItems(),
                    tags);
        }
    }

    public void removeManagedRole(Role role) throws NotFoundByIdException {
        List<Role> roles = roleRepository.findAllByManagedRolesIn(List.of(role));
        for(Role managedRole : roles){
            List<Role> managedRoles = new ArrayList<>(managedRole.getManagedRoles());
            managedRoles.remove(role);
            RoleUpdateDTO roleUpdateDTO = new RoleUpdateDTO(role.getName(),
                    role.getManagedItems().stream().map(Item::getId).toList(),
                    managedRoles.stream().map(Role::getId).toList(),
                    role.getManagedLocations().stream().map(Location::getId).toList(),
                    role.getManagedTags().stream().map(Tag::getId).toList(),
                    role.isAdmin());
            update(managedRole.getId(), roleUpdateDTO, managedRoles,managedRole.getManagedLocations(),
                    managedRole.getManagedItems(), managedRole.getManagedTags());
        }
    }

    @Transactional
    public void addChildLocation(Location parentLocation, Location childLocation){
        List<Role> rolesManagingParent = roleRepository.findAllByManagedLocationsIn(Collections.singletonList(parentLocation));
        for(Role role : rolesManagingParent){
            if(!role.getManagedLocations().contains(childLocation)) {
                role.getManagedLocations().add(childLocation);
            }
        }
    }

    @Transactional
    public void addItemToTags(Item item, List<Tag> tags){
        for(Tag tag : tags){
            List<Role> rolesManagingTag = roleRepository.findAllByManagedTagsIn(List.of(tag));
            for(Role role : rolesManagingTag){
                if(!role.getManagedItems().contains(item)){
                    role.getManagedItems().add(item);
                }
            }
        }
    }

    @Transactional
    public void addItemToLocation(Item item, Location location){
        List<Role> rolesManagingLocation = roleRepository.findAllByManagedLocationsIn(List.of(location));
        for(Role role : rolesManagingLocation){
            if(!role.getManagedItems().contains(item)){
                role.getManagedItems().add(item);
            }
        }

    }

    public List<Role> findParentRoles(Role role){
        return roleRepository.findAllByManagedRolesIn(List.of(role));
    }

}

