package cz.cvut.fit.sh.reservation_system_server.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_IMAGE")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Image {

    public Image(String format) {
        this.format = format;
    }

    @GeneratedValue
    @Id
    private int id;

    @Column(nullable = false)
    private String format;

}
