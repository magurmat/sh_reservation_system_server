package cz.cvut.fit.sh.reservation_system_server.entity;

import cz.cvut.fit.sh.reservation_system_server.entity.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "T_REFRESH_TOKEN")
@Getter
@NoArgsConstructor
public class RefreshToken {

    public RefreshToken(User user, String token, LocalDateTime expiryDate) {
        this.user = user;
        this.token = token;
        this.expiryDate = expiryDate;
    }

    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    @Setter
    private User user;

    @Column(nullable = false, unique = true)
    @Setter
    private String token;

    @Column(nullable = false)
    @Setter
    private LocalDateTime expiryDate;
}
