package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.LocationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.IllegalLocationParentException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundBySlugException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnsupportedImageFormatException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class LocationFacade {

    private final LocationService locationService;
    private final ItemService itemService;
    private final RoleService roleService;
    private final ReservationService reservationService;
    private final ImageService imageService;
    private final SecurityManager securityManager;

    @Autowired
    public LocationFacade(LocationService locationService, ItemService itemService, RoleService roleService,
                          ReservationService reservationService, ImageService imageService, SecurityManager securityManager) {
        this.locationService = locationService;
        this.itemService = itemService;
        this.roleService = roleService;
        this.reservationService = reservationService;
        this.imageService = imageService;
        this.securityManager = securityManager;
    }

    public List<LocationDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllLocationsAccess(SecurityContextHolder.getContext().getAuthentication())){
            return locationService.findAll().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to locations!");
    }

    public List<LocationDTO> findAllParents(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getAllLocationsAccess(SecurityContextHolder.getContext().getAuthentication())){
            Location location = locationService.findById(id).orElseThrow(() ->new NotFoundByIdException(id,"location"));
            return locationService.getLocationsAbove(location).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to locations!");
    }

    public List<LocationDTO> findAllChildren(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getAllLocationsAccess(SecurityContextHolder.getContext().getAuthentication())){
            Location location = locationService.findById(id).orElseThrow(() ->new NotFoundByIdException(id,"location"));
            return locationService.getAllChildLocations(location).stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to locations!");
    }

    public List<LocationDTO> findByIds(List<Integer> ids) throws UnauthorizedException {
        for(Integer locationId : ids){
            if(!securityManager.getLocationAccess(locationId, SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(locationId,"location");
            }
        }
        return locationService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public LocationDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getLocationAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(locationService.findById(id).orElseThrow(() ->new NotFoundByIdException(id,"location")));
        }
        throw new UnauthorizedException(id,"location");
    }

    public LocationDTO findBySlug(String slug) throws UnauthorizedException, NotFoundBySlugException {
        Location location = locationService.findBySlug(slug).orElseThrow(() -> new NotFoundBySlugException(slug,"Location"));
        if(securityManager.getLocationAccess(location.getId(), SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(location);
        }
        throw new UnauthorizedException(slug,"location");
    }

    @Transactional
    public LocationDTO update(int id, LocationUpdateDTO locationUpdateDTO) throws NotFoundByIdException, UnauthorizedException, IllegalLocationParentException, CannotSaveImageException, UnsupportedImageFormatException {
        Optional<Location> optionalLocation = locationService.findById(id);
        if (optionalLocation.isEmpty())
            throw new NotFoundByIdException(id, "Location");
        Location location = optionalLocation.get();
        Integer oldParentId = null;
        if(location.getParentLocation() != null){
            oldParentId = location.getParentLocation().getId();
        }
        Integer newParentId = locationUpdateDTO.getParentLocationId();
        if(oldParentId == null){
            if(newParentId == null){
                if(!securityManager.updateLocationAccessWithoutParentChange(id, SecurityContextHolder.getContext().getAuthentication())){
                    throw new UnauthorizedException(id,"location");
                }
            }
            else{
                if(!securityManager.updateLocationAccessWithParentChange(id, newParentId,
                        SecurityContextHolder.getContext().getAuthentication())){
                    throw new UnauthorizedException(id,"location");
                }
            }
        }
        else{
            if(oldParentId.equals(newParentId)){
                if(!securityManager.updateLocationAccessWithoutParentChange(id, SecurityContextHolder.getContext().getAuthentication())){
                    throw new UnauthorizedException(id,"location");
                }
            }
            else{
                if(!securityManager.updateLocationAccessWithParentChange(id, newParentId,
                        SecurityContextHolder.getContext().getAuthentication())){
                    throw new UnauthorizedException(id,"location");
                }
            }
        }

        if(newParentId != null){
            optionalLocation = locationService.findById(newParentId);
            if (optionalLocation.isEmpty())
                throw new NotFoundByIdException(newParentId, "Location");
            roleService.addChildLocation(optionalLocation.get(), location);
        }

        Image image = null;
        if(locationUpdateDTO.getImage() != null) {
            image = imageService.create(locationUpdateDTO.getImage());
            if(location.getImage() != null){
                imageService.deleteImage(location.getImage());
            }
        }
        else if (location.getImage() != null){
            image = location.getImage();
        }
        return toDTO(locationService.update(id, locationUpdateDTO, image));
    }

    @Transactional
    public LocationDTO create(LocationCreateDTO locationCreateDTO) throws NotFoundByIdException, CannotSaveImageException, UnauthorizedException, UnsupportedImageFormatException {
        if(securityManager.createLocationAccess(locationCreateDTO.getParentLocationId(),
                SecurityContextHolder.getContext().getAuthentication())){
            Image image = null;
            if(locationCreateDTO.getImage() != null) {
                image = imageService.create(locationCreateDTO.getImage());
            }

            Location location = locationService.create(locationCreateDTO, image);


            Optional<Location> parentLocation;
            if(locationCreateDTO.getParentLocationId() != null){
                parentLocation = locationService.findById(locationCreateDTO.getParentLocationId());
                if (parentLocation.isEmpty())
                    throw new NotFoundByIdException(locationCreateDTO.getParentLocationId(), "Location");
                roleService.addChildLocation(parentLocation.get(), location);
            }
            return toDTO(location);
        }
        throw new UnauthorizedException();

    }

    @Transactional
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<Location> optionalLocation = locationService.findById(id);
        if(optionalLocation.isEmpty()){
            throw new NotFoundByIdException(id, "Location");
        }
        if(!securityManager.deleteLocationAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"location");
        }

        Location location = optionalLocation.get();

        roleService.removeManagedLocation(location);
        List<Location> childLocations = new ArrayList<>(locationService.getDirectChildLocations(location));
        for(Location childLocation : childLocations){
            deleteById(childLocation.getId());
        }
        List<Item> items = new ArrayList<>(itemService.findByLocation(location));
        for(Item item : items){
            roleService.removeManagedItem(item);
            List<Reservation> reservations = new ArrayList<>(reservationService.findByItemId(item.getId()));
            for(Reservation reservationDTO : reservations){
                reservationService.deleteById(reservationDTO.getId());
            }
            itemService.deleteById(item.getId());
        }
        locationService.deleteById(location.getId());
    }


    private LocationDTO toDTO(Location location) {
        Integer parentId = null;
        if(location.getParentLocation() != null){
            parentId = location.getParentLocation().getId();
        }

        Integer imageId = null;
        if(location.getImage() != null){
            imageId = location.getImage().getId();
        }
        return new LocationDTO(location.getId(),
                parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                imageId);
    }

    private Optional<LocationDTO> toDTO(Optional<Location> optionalLocation) {
        if(optionalLocation.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalLocation.get()));
    }
}
