package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.LocationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.exception.IllegalLocationParentException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.LocationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class LocationService {

    private final LocationRepository locationRepository;
    private static final Logger logger = LoggerFactory.getLogger(LocationService.class);

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> findAll() {
        return locationRepository.findAll();
    }

    public List<Location> findByIds(List<Integer> ids) {
        return locationRepository.findAllById(ids);
    }

    public Optional<Location> findById(int id) {
        return locationRepository.findById(id);
    }

    public Optional<Location> findBySlug(String slug) {
        return locationRepository.findBySlug(slug);
    }

    @Transactional
    public Location update(int id, LocationUpdateDTO locationUpdateDTO, Image image) throws NotFoundByIdException, IllegalLocationParentException {
        Optional<Location> optionalLocation = findById(id);
        if (optionalLocation.isEmpty())
            throw new NotFoundByIdException(id, "Location");
        Location location = optionalLocation.get();
        Location parentLocation = null;
        if(locationUpdateDTO.getParentLocationId() != null){
            optionalLocation = findById(locationUpdateDTO.getParentLocationId());
            if (optionalLocation.isEmpty())
                throw new NotFoundByIdException(id, "Location");
            parentLocation = optionalLocation.get();
        }

        if(location.equals(parentLocation)){
            throw new IllegalLocationParentException();
        }
        if(getAllChildLocations(location).contains(parentLocation)){
            throw new IllegalLocationParentException();
        }
        logger.info("Updating location with ID: {}.", id);

        location.setName(locationUpdateDTO.getName());
        location.setDescription(locationUpdateDTO.getDescription());
        location.setParentLocation(parentLocation);
        location.setImage(image);
        location.setSlug(locationUpdateDTO.getSlug());
        locationRepository.save(location);
        return location;
    }

    @Transactional
    public Location create(LocationCreateDTO locationCreateDTO, Image image) throws NotFoundByIdException {
        Location parentLocation = null;
        if(locationCreateDTO.getParentLocationId() != null) {
            Optional<Location> parentLocationOptional = findById(locationCreateDTO.getParentLocationId());
            if (parentLocationOptional.isEmpty()) {
                throw new NotFoundByIdException(locationCreateDTO.getParentLocationId(), "Location");
            }
            parentLocation = parentLocationOptional.get();
        }

        logger.info("Creating new location.");
        Location location = new Location(parentLocation,
                locationCreateDTO.getName(),
                locationCreateDTO.getDescription(),
                locationCreateDTO.getSlug(),
                image);
        return locationRepository.save(location);
    }




    @Transactional
    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Location> optionalLocation = locationRepository.findById(id);
        if(optionalLocation.isEmpty())
            throw new NotFoundByIdException(id, "Location");

        logger.info("Deleting location with ID: {}.", id);

        locationRepository.deleteById(id);
    }

    public List<Location> getLocationsAbove(Location location){
        List<Location> result = new ArrayList<>();
        result.add(location);
        Location tmpLocation = location;
        while(tmpLocation.getParentLocation() != null){
            tmpLocation = tmpLocation.getParentLocation();
            result.add(tmpLocation);
        }
        return result;
    }

    public List<Location> getAllChildLocations(Location location){

        List<Location> result = new ArrayList<>(locationRepository.findAllByParentLocation(location));
        List<Location> lastLevel = new ArrayList<>(result), newLocations;
        while(!lastLevel.isEmpty()){
            newLocations = new ArrayList<>();
            for(Location childLocation : lastLevel){
                for(Location newLocation : locationRepository.findAllByParentLocation(childLocation)) {
                    if(!newLocations.contains(newLocation) && !lastLevel.contains(newLocation) && !result.contains(newLocation)) {
                        newLocations.add(newLocation);
                    }
                }
            }
            for(Location lastLevelLocation : lastLevel){
                if(!result.contains(lastLevelLocation)){
                    result.add(lastLevelLocation);
                }
            }
            lastLevel = newLocations;
        }
        return result;
    }

    public List<Location> getDirectChildLocations(Location location){
        return locationRepository.findAllByParentLocation(location);
    }
}

