package cz.cvut.fit.sh.reservation_system_server.exception;

public class CannotReadImageException extends Exception{
    public CannotReadImageException ()
    {
        super("Cannot read image!");
    }
}
