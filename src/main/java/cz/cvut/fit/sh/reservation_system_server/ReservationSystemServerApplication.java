package cz.cvut.fit.sh.reservation_system_server;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ReservationSystemServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ReservationSystemServerApplication.class, args);
	}
}
