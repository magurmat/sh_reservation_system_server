package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdateDTO {

    private String firstName;

    private String lastName;

    private String email;

    private String username;

    private Integer uid;

    private List<Integer> roleIds;

    private boolean active;

    private List<Integer> favouriteIds;
}
