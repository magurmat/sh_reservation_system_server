package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.BanCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Ban;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.RefreshToken;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.EmailAlreadyUsedException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFavouriteItemException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserAlreadyExistsException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserNotFoundException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.BanService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RefreshTokenService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class UserFacade {

    private final UserService userService;
    private final ReservationService reservationService;
    private final RoleService roleService;
    private final ItemService itemService;
    private final BanService banService;
    private final RefreshTokenService refreshTokenService;
    private final SecurityManager securityManager;

    @Autowired
    public UserFacade(UserService userService, ReservationService reservationService, RoleService roleService,
                      ItemService itemService, BanService banService, RefreshTokenService refreshTokenService,
                      SecurityManager securityManager) {
        this.userService = userService;
        this.reservationService = reservationService;
        this.roleService = roleService;
        this.itemService = itemService;
        this.banService = banService;
        this.refreshTokenService = refreshTokenService;
        this.securityManager = securityManager;
    }

    public List<UserDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllUsersAccess(SecurityContextHolder.getContext().getAuthentication())){
            return userService.findAll().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to users!");
    }

    public List<UserDTO> findByIds(List<Integer> ids) throws UnauthorizedException {
        for(Integer userId : ids){
            if(!securityManager.getUserAccess(userId, SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(userId,"user");
            }
        }
        return userService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public UserDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getUserAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(userService.findById(id).orElseThrow(() ->new NotFoundByIdException(id,"User")));
        }
        throw new UnauthorizedException(id,"user");

    }

    public UserDTO findByUsername(String username) throws UnauthorizedException, UserNotFoundException {
        User user = userService.findByUsername(username).orElseThrow(() ->new UserNotFoundException(username));
        if(securityManager.getUserAccess(user.getId(), SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(user);
        }
        throw new UnauthorizedException(user.getId(),"user");
    }

    public UserDTO register(UserCreateDTO user) throws NotFoundByIdException, UserAlreadyExistsException, EmailAlreadyUsedException {
        Optional<User> alreadyExistingUser = userService.findByUsername(user.getUsername());
        if(alreadyExistingUser.isPresent()){
            throw new UserAlreadyExistsException(user.getUsername());
        }
        alreadyExistingUser = userService.findByEmail(user.getEmail());
        if(alreadyExistingUser.isPresent()){
            throw new EmailAlreadyUsedException();
        }
        return create(user);
    }

    @Transactional
    public UserDTO addFavourite(int userId, int itemId) throws NotFoundByIdException, UnauthorizedException {
        if(!securityManager.addUsersFavouriteItemAccess(userId, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(userId,"user");
        }

        Item item = itemService.findById(itemId).orElseThrow(() -> new NotFoundByIdException(itemId, "item"));
        User user = userService.findById(userId).orElseThrow(() -> new NotFoundByIdException(userId, "user"));
        if(!user.getFavourites().contains(item)) {
            user.getFavourites().add(item);
        }
        return toDTO(user);
    }

    @Transactional
    public UserDTO removeFavourite(int userId, int itemId) throws UnauthorizedException, NotFavouriteItemException, NotFoundByIdException {
        if(!securityManager.removeUsersFavouriteItemAccess(userId, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(userId,"user");
        }
        Item item = itemService.findById(itemId).orElseThrow(() -> new NotFoundByIdException(itemId, "item"));
        User user = userService.findById(userId).orElseThrow(() -> new NotFoundByIdException(userId, "user"));
        if(user.getFavourites().contains(item)){
            user.getFavourites().remove(item);
        }
        else{
            throw new NotFavouriteItemException();
        }
        return toDTO(user);
    }

    private UserDTO create(UserCreateDTO userCreateDTO) throws NotFoundByIdException {
        List<Role> roles = new ArrayList<>();
        for (Integer roleId : userCreateDTO.getRoleIds()) {
            Optional<Role> optionalRole = roleService.findById(roleId);
            if (optionalRole.isEmpty())
                throw new NotFoundByIdException(roleId, "Role");
            roles.add(optionalRole.get());
        }
        List<Item> favouriteItems = new ArrayList<>();
        if(userCreateDTO.getFavouriteIds() != null){
            for (Integer itemId : userCreateDTO.getFavouriteIds()) {
                Optional<Item> optionalItem = itemService.findById(itemId);
                if (optionalItem.isEmpty())
                    throw new NotFoundByIdException(itemId, "Item");
                favouriteItems.add(optionalItem.get());
            }
        }
        return toDTO(userService.create(userCreateDTO, roles, favouriteItems));
    }

    public UserDTO update(int id, UserUpdateDTO userDTO) throws NotFoundByIdException, UnauthorizedException {
        Optional<User> optionalUser = userService.findById(id);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(id, "User");

        if(!securityManager.updateUserAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"user");
        }

        List<Role> roles = new ArrayList<>();
        for (Integer roleId : userDTO.getRoleIds()) {
            Optional<Role> optionalRole = roleService.findById(roleId);
            if (optionalRole.isEmpty())
                throw new NotFoundByIdException(roleId, "Role");
            roles.add(optionalRole.get());
        }

        List<Item> favouriteItems = new ArrayList<>();
        if(userDTO.getFavouriteIds() != null){
            for (Integer itemId : userDTO.getFavouriteIds()) {
                Optional<Item> optionalItem = itemService.findById(itemId);
                if (optionalItem.isEmpty())
                    throw new NotFoundByIdException(itemId, "Item");
                favouriteItems.add(optionalItem.get());
            }
        }
        return toDTO(userService.update(id, userDTO, roles, favouriteItems));
    }

    @Transactional
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<User> optionalUser = userService.findById(id);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(id, "User");

        if (!securityManager.deleteUserAccess(id, SecurityContextHolder.getContext().getAuthentication())) {
            throw new UnauthorizedException(id, "user");
        }

        List<Reservation> reservations = reservationService.findByUserId(id);
        for (Reservation reservation : reservations) {
            reservationService.deleteById(reservation.getId());
        }
        List<Ban> bans = banService.findByUser(optionalUser.get());
        for (Ban ban : bans) {
            banService.deleteById(ban.getId());
        }

        List<RefreshToken> tokens = refreshTokenService.findByUser(optionalUser.get());
        for (RefreshToken token : tokens) {
            refreshTokenService.delete(token.getId());
        }
        userService.deleteById(id);
    }

    public Ban userBanned(int userId) throws NotFoundByIdException {
        Optional<User> optionalUser = userService.findById(userId);
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(userId, "User");


        List<Ban> bans = banService.findByUser(optionalUser.get());
        for(Ban ban : bans){
            if(ban.getExpirationDate().isAfter(LocalDateTime.now())){
                return ban;
            }
        }
        return null;
    }

    public void banUser(BanCreateDTO ban) throws UnauthorizedException, NotFoundByIdException {
        Optional<User> optionalUser = userService.findById(ban.getUserId());
        if (optionalUser.isEmpty())
            throw new NotFoundByIdException(ban.getUserId(), "User");

        if(!securityManager.banUserAccess(SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(ban.getUserId(),"user");
        }
        banService.create(ban, optionalUser.get());
    }

    private UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getPassword(), user.getUsername(), user.getUid(),
                user.getRoles().stream().map(Role::getId).toList(),
                user.getFavourites().stream().map(Item::getId).toList(),
                user.isActive());
    }

    private Optional<UserDTO> toDTO(Optional<User> user) {
        if (user.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(user.get()));
    }

}
