package cz.cvut.fit.sh.reservation_system_server.repository;

import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TagRepository extends JpaRepository<Tag, Integer> {
}
