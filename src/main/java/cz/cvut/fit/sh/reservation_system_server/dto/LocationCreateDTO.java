package cz.cvut.fit.sh.reservation_system_server.dto;

import cz.cvut.fit.sh.reservation_system_server.validation.KebabCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class LocationCreateDTO {

    private Integer parentLocationId;

    private String name;

    private String description;

    @KebabCase
    private String slug;

    private MultipartFile image;
}
