package cz.cvut.fit.sh.reservation_system_server.exception;

public class UserNotFoundException extends Exception{
    public UserNotFoundException(String username)
    {
        super("User with username " + username + " was not found!");
    }
}
