package cz.cvut.fit.sh.reservation_system_server.entity;

public enum ReservationState {
    CREATED,
    CONFIRMED,
    DENIED,
    CANCELLED,
    COMPLETED
}
