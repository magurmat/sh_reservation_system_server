package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.RoleDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.RoleUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.facade.RoleFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/role")
public class RoleController {

    private final RoleFacade roleFacade;

    public RoleController(RoleFacade roleFacade) {
        this.roleFacade = roleFacade;
    }

    @GetMapping
    ResponseEntity<List<RoleDTO>> allRoles() throws UnauthorizedException {
        return new ResponseEntity<>(roleFacade.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<RoleDTO> findRoleById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(roleFacade.findById(id), HttpStatus.OK);
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<RoleDTO> saveRole(@RequestBody RoleCreateDTO role) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(roleFacade.create(role), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<RoleDTO> updateRole(@PathVariable int id, @RequestBody RoleUpdateDTO role) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(roleFacade.update(id, role), HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteRole(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        roleFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
