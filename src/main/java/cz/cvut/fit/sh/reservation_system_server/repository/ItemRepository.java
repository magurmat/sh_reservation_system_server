package cz.cvut.fit.sh.reservation_system_server.repository;


import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ItemRepository extends JpaRepository<Item, Integer> {

    List<Item> findAllByLocation(Location location);

    List<Item> findAllByTagsIn(List<Tag> Tags);

    Optional<Item> findBySlug(String slug);
}
