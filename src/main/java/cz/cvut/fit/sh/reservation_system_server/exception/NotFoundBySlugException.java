package cz.cvut.fit.sh.reservation_system_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class NotFoundBySlugException extends Exception{
    public NotFoundBySlugException(String slug, String entity)
    {
        super(entity + " with slug " + slug + " was not found!");
    }
}