package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.ImageDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotReadImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ImageFacade {

    private final ImageService imageService;

    @Autowired
    public ImageFacade(ImageService imageService) {
        this.imageService = imageService;
    }

    public ImageDTO findById(int id) throws CannotReadImageException, NotFoundByIdException {
        Image image = imageService.findById(id).orElseThrow(() -> new NotFoundByIdException(id, "image"));
        return new ImageDTO(imageService.loadImageDataById(id, image.getFormat()), image.getFormat());
    }
}
