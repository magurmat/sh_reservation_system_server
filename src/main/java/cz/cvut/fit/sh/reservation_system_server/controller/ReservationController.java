package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ReservationDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ReservationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemDisabledException;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemOccupiedException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.facade.ReservationFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Set;


@RestController
@CrossOrigin("*")
public class ReservationController {

    private final ReservationFacade reservationFacade;

    public ReservationController(ReservationFacade reservationFacade) {
        this.reservationFacade = reservationFacade;
    }

    @GetMapping
    ResponseEntity<List<ReservationDTO>> allReservations() throws UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findAll(), HttpStatus.OK);
    }

    @GetMapping("/reservation/{id}")
    ResponseEntity<ReservationDTO> findReservationById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findById(id), HttpStatus.OK);
    }

    @GetMapping("/reservation/user/{id}")
    ResponseEntity<List<ReservationDTO>> findReservationByUserId(@PathVariable int id) throws UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findByUserId(id), HttpStatus.OK);
    }

    @GetMapping("/reservation/item/{id}")
    ResponseEntity<List<ReservationDTO>> findReservationByItemId(@PathVariable int id) throws UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findByItemId(id), HttpStatus.OK);
    }


    @PostMapping("/reservation")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<ReservationDTO> saveReservation(@RequestBody ReservationCreateDTO reservation) throws UnauthorizedException, NotFoundByIdException, ItemOccupiedException, ItemDisabledException {
        return new ResponseEntity<>(reservationFacade.create(reservation), HttpStatus.OK);

    }

    @PutMapping("/reservation/{id}")
    ResponseEntity<ReservationDTO> updateReservation(@PathVariable int id, @RequestBody ReservationUpdateDTO reservation) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.update(id, reservation), HttpStatus.OK);
    }

    @GetMapping("/user/{id}/reservation/waiting")
    ResponseEntity<Set<ReservationDTO>> unconfirmedReservationsForUser(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findUnconfirmedReservationsForUser(id), HttpStatus.OK);
    }

    @GetMapping("/user/{id}/reservation/upcoming")
    ResponseEntity<Set<ReservationDTO>> upcomingUserReservations(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findUpcomingReservationsForUser(id), HttpStatus.OK);
    }

    @GetMapping("/item/{id}/reservation/upcoming")
    ResponseEntity<Set<ReservationDTO>> upcomingItemReservations(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(reservationFacade.findUpcomingReservationsForItem(id), HttpStatus.OK);
    }
}
