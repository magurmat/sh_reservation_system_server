package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

import java.util.List;

@Data
public class UserDTO {

    private final int id;

    private final String firstName;

    private final String lastName;

    private final String email;

    private final String password;

    private final String username;

    private final Integer uid;

    private final List<Integer> roleIds;

    private final List<Integer> favouriteIds;

    private final boolean active;
}
