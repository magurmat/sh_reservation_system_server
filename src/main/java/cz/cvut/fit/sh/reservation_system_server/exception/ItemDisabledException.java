package cz.cvut.fit.sh.reservation_system_server.exception;

public class ItemDisabledException extends Exception{
    public ItemDisabledException ()
    {
        super("Item is not ready to be reserved!");
    }
}
