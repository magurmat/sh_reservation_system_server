package cz.cvut.fit.sh.reservation_system_server.exception;

public class ExpiredRefreshTokenException extends Exception{
    public ExpiredRefreshTokenException() {
        super("Refresh token was expired. Please make a new signin request");
    }
}
