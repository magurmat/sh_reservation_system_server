package cz.cvut.fit.sh.reservation_system_server.repository;

import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ImageRepository extends JpaRepository<Image, Integer> {

}
