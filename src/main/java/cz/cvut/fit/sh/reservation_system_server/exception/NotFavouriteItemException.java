package cz.cvut.fit.sh.reservation_system_server.exception;

public class NotFavouriteItemException extends Exception{
    public NotFavouriteItemException ()
    {
        super("Item isnt users favourite!");
    }
}
