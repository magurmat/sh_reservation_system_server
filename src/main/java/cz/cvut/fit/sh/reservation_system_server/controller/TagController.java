package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.TagCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.TagDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.TagUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.facade.TagFacade;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/tag")
public class TagController {

    private final TagFacade tagFacade;

    public TagController(TagFacade tagFacade) {
        this.tagFacade = tagFacade;
    }

    @GetMapping
    ResponseEntity<List<TagDTO>> allTags() throws UnauthorizedException {
        return new ResponseEntity<>(tagFacade.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<TagDTO> findTagById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(tagFacade.findById(id), HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<TagDTO> saveTag(@Valid @RequestBody TagCreateDTO tag) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(tagFacade.create(tag), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<TagDTO> updateTag(@PathVariable int id, @RequestBody TagUpdateDTO tag) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(tagFacade.update(id, tag), HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteTag(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        tagFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}

