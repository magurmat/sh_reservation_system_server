package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

import java.util.List;

@Data
public class RoleDTO {

    private final int id;

    private final String name;

    private final List<Integer> managedItemIds;

    private final List<Integer> managedRoleIds;

    private final List<Integer> managedLocationIds;

    private final List<Integer> managedTagIds;

    private final boolean isAdmin;
}
