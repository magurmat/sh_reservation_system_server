package cz.cvut.fit.sh.reservation_system_server.repository;


import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface LocationRepository extends JpaRepository<Location, Integer> {

    List<Location> findAllByParentLocation(Location parentLocations);

    Optional<Location> findBySlug(String slug);
}
