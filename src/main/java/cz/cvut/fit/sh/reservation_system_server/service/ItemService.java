package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.ItemRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ItemService {

    private final ItemRepository itemRepository;
    private static final Logger logger = LoggerFactory.getLogger(ItemService.class);

    @Autowired
    public ItemService(ItemRepository itemRepository) {
        this.itemRepository = itemRepository;
    }

    public List<Item> findAll() {
        return itemRepository.findAll();
    }

    public List<Item> findByIds(List<Integer> ids) {
        return itemRepository.findAllById(ids);
    }

    public Optional<Item> findById(int id) {
        return itemRepository.findById(id);
    }

    public Optional<Item> findBySlug(String slug) {
        return itemRepository.findBySlug(slug);
    }

    public Item create(ItemCreateDTO itemCreateDTO, Location location, List<Tag> tags, Image image){
        logger.info("Creating new item.");
        return itemRepository.save(
                new Item(
                        itemCreateDTO.getDescription(),
                        itemCreateDTO.getName(),
                        location,
                        itemCreateDTO.getSlug(),
                        tags,
                        image,
                        itemCreateDTO.getEnabled()
                )
        );
    }

    public Item update(int id, ItemUpdateDTO itemDTO, Location location, List<Tag> tags, Image image) throws NotFoundByIdException {
        Optional<Item> optionalItem = itemRepository.findById(id);
        if (optionalItem.isEmpty())
            throw new NotFoundByIdException(id, "Item");

        logger.info("Updating item with ID: {}.", id);

        Item item = optionalItem.get();
        item.setDescription(itemDTO.getDescription());
        item.setName(itemDTO.getName());
        item.setLocation(location);
        item.setTags(tags);
        item.setSlug(itemDTO.getSlug());
        item.setImage(image);
        item.setEnabled(itemDTO.getEnabled());
        itemRepository.save(item);
        return item;
    }

    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Item> optionalItem = itemRepository.findById(id);
        if(optionalItem.isEmpty()) {
            throw new NotFoundByIdException(id, "Item");
        }

        logger.info("Deleting item with ID: {}.", id);

        itemRepository.deleteById(id);
    }

    @Transactional
    public void removeTag(Tag tag){
        List<Item> items = findByTag(tag);
        for(Item item : items){
            item.getTags().remove(tag);
        }
    }

    public List<Item> findByLocation(Location location){
        return itemRepository.findAllByLocation(location);
    }

    public List<Item> findByTag(Tag tag){
        return itemRepository.findAllByTagsIn(List.of(tag));
    }
}
