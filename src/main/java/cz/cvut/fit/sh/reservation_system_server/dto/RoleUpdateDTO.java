package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RoleUpdateDTO {

    private String name;

    private List<Integer> managedItemIds;

    private List<Integer> managedRoleIds;

    private List<Integer> managedLocationIds;

    private List<Integer> managedTagIds;

    private boolean isAdmin;
}
