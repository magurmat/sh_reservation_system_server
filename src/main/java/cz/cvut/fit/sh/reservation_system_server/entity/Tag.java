package cz.cvut.fit.sh.reservation_system_server.entity;

import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "T_TAG")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Tag {

    public Tag(String name) {
        this.name = name;
    }

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Setter
    @Column(nullable = false, unique = true)
    private String name;
}
