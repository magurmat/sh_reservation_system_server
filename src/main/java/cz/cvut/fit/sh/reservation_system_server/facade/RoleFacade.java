package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.RoleDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.RoleUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@Component
public class RoleFacade {
    private final RoleService roleService;
    private final ItemService itemService;
    private final LocationService locationService;
    private final UserService userService;
    private final TagService tagService;
    private final SecurityManager securityManager;

    @Autowired
    public RoleFacade(RoleService roleService, ItemService itemService, LocationService locationService, UserService userService, TagService tagService, SecurityManager securityManager) {
        this.roleService = roleService;
        this.itemService = itemService;
        this.locationService = locationService;
        this.userService = userService;
        this.tagService = tagService;
        this.securityManager = securityManager;
    }

    public List<RoleDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllRolesAccess(SecurityContextHolder.getContext().getAuthentication())){
            return roleService
                    .findAll()
                    .stream()
                    .map(this::toDTO)
                    .toList();
        }
        throw new UnauthorizedException("No access to roles!");

    }

    public List<RoleDTO> findByIds(List<Integer> ids) throws UnauthorizedException {
        for(Integer roleId : ids){
            if(!securityManager.getRoleAccess(roleId, SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(roleId,"role");
            }
        }
        return roleService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public RoleDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getRoleAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(roleService.findById(id)).orElseThrow(() ->new NotFoundByIdException(id,"Role"));
        }
        throw new UnauthorizedException(id,"role");
    }

    @Transactional
    public RoleDTO update(int id, RoleUpdateDTO roleUpdateDTO) throws NotFoundByIdException, UnauthorizedException {
        Optional<Role> optionalRole = roleService.findById(id);
        if (optionalRole.isEmpty())
            throw new NotFoundByIdException(id, "Role");

        if(!securityManager.updateRoleAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"role");
        }
        List<Role> managedRoles =  loadManagedRoles(roleUpdateDTO.getManagedRoleIds());

        List<Location> managedLocations = loadManagedLocations(roleUpdateDTO.getManagedLocationIds(), managedRoles);

        List<Tag> managedTags = loadManagedTags(roleUpdateDTO.getManagedTagIds(), managedRoles);

        List<Item> managedItems = loadManagedItems(roleUpdateDTO.getManagedItemIds(), managedRoles, managedLocations, managedTags);

        Role result = roleService.update(id, roleUpdateDTO, managedRoles, managedLocations, managedItems, managedTags);

        //TODO control if this works fine
        List<Role> parentRoles = roleService.findParentRoles(optionalRole.get());
        for(Role parentRole : parentRoles){
            RoleUpdateDTO parentUpdateDTO = new RoleUpdateDTO(parentRole.getName(),
                    parentRole.getManagedItems().stream().map(Item::getId).toList(),
                    parentRole.getManagedRoles().stream().map(Role::getId).toList(),
                    parentRole.getManagedLocations().stream().map(Location::getId).toList(),
                    parentRole.getManagedTags().stream().map(Tag::getId).toList(),
                    parentRole.isAdmin());

            managedRoles =  loadManagedRoles(parentUpdateDTO.getManagedRoleIds());
            managedLocations = loadManagedLocations(parentUpdateDTO.getManagedLocationIds(), managedRoles);
            managedTags = loadManagedTags(parentUpdateDTO.getManagedTagIds(), managedRoles);
            managedItems = loadManagedItems(parentUpdateDTO.getManagedItemIds(), managedRoles, managedLocations, managedTags);
            roleService.update(parentRole.getId(), parentUpdateDTO, managedRoles, managedLocations, managedItems, managedTags);

            update(parentRole.getId(), parentUpdateDTO);
        }
        return toDTO(result);
    }

    public RoleDTO create(RoleCreateDTO roleCreateDTO) throws NotFoundByIdException, UnauthorizedException {
        if(!securityManager.createRoleAccess(roleCreateDTO, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException();
        }
        List<Role> managedRoles =  loadManagedRoles(roleCreateDTO.getManagedRoleIds());

        List<Location> managedLocations = loadManagedLocations(roleCreateDTO.getManagedLocationIds(), managedRoles);

        List<Tag> managedTags = loadManagedTags(roleCreateDTO.getManagedTagIds(), managedRoles);

        List<Item> managedItems = loadManagedItems(roleCreateDTO.getManagedItemIds(), managedRoles, managedLocations, managedTags);

        return toDTO(roleService.create(roleCreateDTO, managedRoles, managedLocations, managedItems, managedTags));
    }


    @Transactional
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<Role> optionalRole = roleService.findById(id);
        if(optionalRole.isEmpty())
            throw new NotFoundByIdException(id, "Role");
        if(!securityManager.deleteRoleAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            throw new UnauthorizedException(id,"role");
        }
        Role role = optionalRole.get();

        List<User> users = userService.findByRole(role);
        for(User user : users){
            userService.removeRoleFromUser(user, role);
        }

        roleService.removeManagedRole(role);
        roleService.deleteById(id);
    }

    private List<Role> loadManagedRoles(List<Integer> managedRoleIds) throws NotFoundByIdException {
        List<Role> result = new ArrayList<>();
        if(managedRoleIds == null || managedRoleIds.isEmpty()){
            return result;
        }
        for(Integer managedRoleId : managedRoleIds){
            Optional<Role> optionalManagedRole = roleService.findById(managedRoleId);
            if (optionalManagedRole.isEmpty())
                throw new NotFoundByIdException(managedRoleId, "Managed role");
            Role managedRole = optionalManagedRole.get();
            if(!result.contains(managedRole)) {
                result.add(managedRole);
            }

            for(Role tmpRole : managedRole.getManagedRoles()){
                if(!result.contains(tmpRole)){
                    result.add(tmpRole);
                }
            }
        }
        return result;
    }

    private List<Location> loadManagedLocations(List<Integer> managedLocationIds, List<Role> managedRoles) throws NotFoundByIdException {
        List<Location> result = new ArrayList<>();

        if(managedLocationIds != null && !managedLocationIds.isEmpty()){
            for(Integer managedLocationId : managedLocationIds){
                Optional<Location> optionalLocation = locationService.findById(managedLocationId);
                if (optionalLocation.isEmpty())
                    throw new NotFoundByIdException(managedLocationId, "Managed location");
                result.add(optionalLocation.get());
                for(Location childLocation : locationService.getAllChildLocations(optionalLocation.get())){
                    if(!result.contains(childLocation)){
                        result.add(childLocation);
                    }
                }
            }
        }
        for(Role managedRole : managedRoles){
            for(Location managedLocation : managedRole.getManagedLocations()){
                if(!result.contains(managedLocation)){
                    result.add(managedLocation);
                }
            }
        }

        return result;
    }

    private List<Tag> loadManagedTags(List<Integer> managedTagIds, List<Role> managedRoles) throws NotFoundByIdException {
        List<Tag> result = new ArrayList<>();

        if(managedTagIds != null && !managedTagIds.isEmpty()){
            for(Integer managedTagId : managedTagIds){
                Optional<Tag> optionalTag = tagService.findById(managedTagId);
                if (optionalTag.isEmpty())
                    throw new NotFoundByIdException(managedTagId, "Managed tag");
                result.add(optionalTag.get());
            }
        }
        for(Role managedRole : managedRoles){
            for(Tag managedTag : managedRole.getManagedTags()){
                if(!result.contains(managedTag)){
                    result.add(managedTag);
                }
            }
        }

        return result;
    }

    private List<Item> loadManagedItems(List<Integer> managedItemIds, List<Role> managedRoles,
                                        List<Location> managedLocations, List<Tag> managedTags) throws NotFoundByIdException {
        List<Item> result = new ArrayList<>();

        if(managedItemIds != null && !managedItemIds.isEmpty()){
            for(Integer managedItemId : managedItemIds){
                Optional<Item> optionalItem = itemService.findById(managedItemId);
                if (optionalItem.isEmpty())
                    throw new NotFoundByIdException(managedItemId, "Managed item");
                if(!result.contains(optionalItem.get())) {
                    result.add(optionalItem.get());
                }
            }
        }
        //through roles
        for(Role managedRole : managedRoles){
            for(Item managedItem : managedRole.getManagedItems()){
                if(!result.contains(managedItem)){
                    result.add(managedItem);
                }
            }
        }
        //through locations
        for(Location managedLocation : managedLocations){
            for(Item managedItem : itemService.findByLocation(managedLocation)){
                if(!result.contains(managedItem)){
                    result.add(managedItem);
                }
            }
        }
        //through tags
        for(Tag managedTag : managedTags){
            for(Item managedItem : itemService.findByTag(managedTag)){
                if(!result.contains(managedItem)){
                    result.add(managedItem);
                }
            }
        }
        return result;
    }

    private RoleDTO toDTO(Role role) {
        return new RoleDTO(role.getId(),
                role.getName(),
                role.getManagedItems().stream().map(Item::getId).toList(),
                role.getManagedRoles().stream().map(Role::getId).toList(),
                role.getManagedLocations().stream().map(Location::getId).toList(),
                role.getManagedTags().stream().map(Tag::getId).toList(),
                role.isAdmin());
    }

    private Optional<RoleDTO> toDTO(Optional<Role> optionalRole) {
        if(optionalRole.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(optionalRole.get()));
    }

}
