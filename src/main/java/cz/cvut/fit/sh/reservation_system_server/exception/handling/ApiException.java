package cz.cvut.fit.sh.reservation_system_server.exception.handling;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@AllArgsConstructor
@Getter
@Setter
public class ApiException{

    HttpStatus status;
    String message;
    LocalDateTime timestamp;
}
