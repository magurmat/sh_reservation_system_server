package cz.cvut.fit.sh.reservation_system_server.exception;

public class ItemOccupiedException extends Exception{
    public ItemOccupiedException ()
    {
        super("Item is already reserved in given time!");
    }
}
