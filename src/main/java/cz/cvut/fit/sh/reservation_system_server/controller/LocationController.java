package cz.cvut.fit.sh.reservation_system_server.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.IllegalLocationParentException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnsupportedImageFormatException;
import cz.cvut.fit.sh.reservation_system_server.facade.LocationFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/location")
public class LocationController {

    private final LocationFacade locationFacade;

    @Autowired
    public LocationController(LocationFacade locationFacade) {
        this.locationFacade = locationFacade;
    }

    @GetMapping
    ResponseEntity<List<LocationDTO>> allLocations() throws UnauthorizedException {
        return new ResponseEntity<>(locationFacade.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}/child")
    ResponseEntity<List<LocationDTO>> allLocationChildren(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(locationFacade.findAllChildren(id), HttpStatus.OK);
    }

    @GetMapping("/{id}/parent")
    ResponseEntity<List<LocationDTO>> allLocationParents(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(locationFacade.findAllParents(id), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<LocationDTO> findLocationById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(locationFacade.findById(id), HttpStatus.OK);
    }

    @PostMapping(consumes = "multipart/form-data")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<LocationDTO> saveLocation(@Valid @RequestPart("data") LocationCreateDTO location,
                              @RequestPart(name = "image", required = false)MultipartFile multipartFile)
            throws CannotSaveImageException, NotFoundByIdException, UnauthorizedException, UnsupportedImageFormatException {
        if(multipartFile != null) {
            location.setImage(multipartFile);
        }
        return new ResponseEntity<>(locationFacade.create(location), HttpStatus.OK);
    }

    @PutMapping(value = "/{id}", consumes = "multipart/form-data")
    ResponseEntity<LocationDTO> updateLocation(@PathVariable int id,
                                               @Valid  @RequestPart("data") LocationUpdateDTO location,
                                               @RequestPart(name = "image", required = false)MultipartFile multipartFile)
            throws UnauthorizedException, NotFoundByIdException, IllegalLocationParentException, CannotSaveImageException, UnsupportedImageFormatException {
        if(multipartFile != null) {
            location.setImage(multipartFile);
        }
        return new ResponseEntity<>(locationFacade.update(id,location), HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteLocation(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        locationFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}
