package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class BanDTO {

    private final int id;

    private final String userId;

    private final LocalDateTime expirationDate;
}