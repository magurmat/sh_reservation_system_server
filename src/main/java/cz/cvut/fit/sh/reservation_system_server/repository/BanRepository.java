package cz.cvut.fit.sh.reservation_system_server.repository;

import cz.cvut.fit.sh.reservation_system_server.entity.Ban;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BanRepository extends JpaRepository<Ban, Integer> {

    List<Ban> findAllByUser(User user);
}