package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ReservationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ReservationService {

    private final ReservationRepository reservationRepository;
    private static final Logger logger = LoggerFactory.getLogger(ReservationService.class);

    public ReservationService(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    public List<Reservation> findAll() {
        return reservationRepository
                .findAll();
    }

    public List<Reservation> findByIds(List<Integer> ids) {
        return reservationRepository.findAllById(ids);
    }

    public Optional<Reservation> findById(int id) {
        return reservationRepository.findById(id);
    }

    public List<Reservation> findByUserId(int id) {
        return reservationRepository.findAllByUserId(id);
    }

    public List<Reservation> findByItemId(int id) {
        return reservationRepository.findAllByItemId(id);
    }

    public Reservation update(int id, ReservationUpdateDTO reservationUpdateDTO, User user,
                              Item item) throws NotFoundByIdException {
        Optional<Reservation> optionalReservation = findById(id);
        if (optionalReservation.isEmpty())
            throw new NotFoundByIdException(id, "Reservation");

        logger.info("Updating reservation with ID: {}.", id);

        Reservation reservation = optionalReservation.get();
        reservation.setEndTime(reservationUpdateDTO.getEndTime());
        reservation.setStartTime(reservationUpdateDTO.getStartTime());
        reservation.setState(reservationUpdateDTO.getState());
        reservation.setUser(user);
        reservation.setItem(item);
        reservationRepository.save(reservation);
        return reservation;
    }

    public Reservation create(ReservationCreateDTO reservationCreateDTO, User user, Item item){
        logger.info("Creating new reservation.");
        Reservation reservation = new Reservation(reservationCreateDTO.getStartTime(),
                reservationCreateDTO.getEndTime(),
                ReservationState.CREATED,
                user,
                item);
        return reservationRepository.save(reservation);
    }

    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Reservation> optionalReservation = reservationRepository.findById(id);
        if(optionalReservation.isEmpty())
            throw new NotFoundByIdException(id, "Reservation");

        logger.info("Deleting reservation with ID: {}.", id);

        reservationRepository.deleteById(id);
    }

}

