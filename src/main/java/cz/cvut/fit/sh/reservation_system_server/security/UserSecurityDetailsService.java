package cz.cvut.fit.sh.reservation_system_server.security;

import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserSecurityDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserSecurityDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> user = userService.findByUsername(userName);
        user.orElseThrow(() -> new UsernameNotFoundException("Not found: " + userName));
        return user.map(UserSecurityDetails::new).get();
    }

    public UserSecurityDetails loadByEmail(String email){
        Optional<User> user = userService.findByEmail(email);
        if(user.isEmpty()){
            return null;
        }
        return user.map(UserSecurityDetails::new).get();
    }
}
