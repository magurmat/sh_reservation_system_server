package cz.cvut.fit.sh.reservation_system_server.security;

import cz.cvut.fit.sh.reservation_system_server.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationManager implements org.springframework.security.authentication.AuthenticationManager {

    @Autowired
    private UserSecurityDetailsService userSecurityDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
        UserSecurityDetails user = userSecurityDetailsService.loadUserByUsername(username);
        if (user == null) {
            throw new BadCredentialsException("User doesnt exist.");
        }
        if (!passwordEncoder.matches(password, user.getPassword())) {
            throw new BadCredentialsException("Bad password.");
        }
        return authentication;
    }

}
