package cz.cvut.fit.sh.reservation_system_server.repository;


import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {

    List<Role> findAllByManagedItemsIn(List<Item> managedItems);

    List<Role> findAllByManagedLocationsIn(List<Location> managedLocations);

    List<Role> findAllByManagedRolesIn(List<Role> managedRoles);

    List<Role> findAllByManagedTagsIn(List<Tag> managedTags);
}
