package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.ImageDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotReadImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.facade.ImageFacade;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin("*")
@RequestMapping("/image")
public class ImageController {

    private final ImageFacade imageFacade;

    public ImageController(ImageFacade imageFacade) {
        this.imageFacade = imageFacade;
    }

    @GetMapping("/{id}")
    ResponseEntity<byte[]> findImageById(@PathVariable int id) throws CannotReadImageException, NotFoundByIdException {
        ImageDTO image = imageFacade.findById(id);
        MediaType format;
        if(image.getFormat().equals("png")){
            format = MediaType.IMAGE_PNG;
        }
        else{
            format = MediaType.IMAGE_JPEG;
        }

        return ResponseEntity
                    .ok()
                    .contentType(format)
                    .body(image.getImageData());
    }
}
