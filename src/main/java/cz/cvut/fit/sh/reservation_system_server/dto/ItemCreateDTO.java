package cz.cvut.fit.sh.reservation_system_server.dto;

import cz.cvut.fit.sh.reservation_system_server.validation.KebabCase;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ItemCreateDTO {

    private String description;

    private String name;

    private Integer locationId;

    @KebabCase
    private String slug;

    private List<Integer> tagIds;

    private MultipartFile image;

    private Boolean enabled;
}
