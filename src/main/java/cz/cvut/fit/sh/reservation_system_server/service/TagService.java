package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.TagCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.TagUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TagService {

    private final TagRepository tagRepository;
    private static final Logger logger = LoggerFactory.getLogger(TagService.class);

    @Autowired

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    public List<Tag> findAll() {
        return tagRepository.findAll();
    }

    public List<Tag> findByIds(List<Integer> ids) {
        return tagRepository.findAllById(ids);
    }

    public Optional<Tag> findById(int id) {
        return tagRepository.findById(id);
    }

    public Tag create(TagCreateDTO tagCreateDTO){
        logger.info("Creating new tag.");
        return tagRepository.save(
                new Tag(
                        tagCreateDTO.getName()
                )
        );
    }

    public Tag update(int id, TagUpdateDTO tagDTO) throws NotFoundByIdException {
        Optional<Tag> optionalTag = tagRepository.findById(id);
        if (optionalTag.isEmpty())
            throw new NotFoundByIdException(id, "Tag");

        logger.info("Updating tag with ID: {}.", id);

        Tag tag = optionalTag.get();
        tag.setName(tagDTO.getName());
        tagRepository.save(tag);
        return tag;
    }

    public void deleteById(int id) throws NotFoundByIdException {
        Optional<Tag> optionalTag = tagRepository.findById(id);
        if(optionalTag.isEmpty()) {
            throw new NotFoundByIdException(id, "Tag");
        }

        logger.info("Deleting tag with ID: {}.", id);

        tagRepository.deleteById(id);
    }
}
