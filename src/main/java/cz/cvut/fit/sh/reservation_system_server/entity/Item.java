package cz.cvut.fit.sh.reservation_system_server.entity;

import cz.cvut.fit.sh.reservation_system_server.validation.KebabCase;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.List;


@Entity
@Table(name = "T_ITEM")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Item {

    public Item(String description, String name, Location location, String slug, List<Tag> tags, Image image, Boolean enabled) {
        this.description = description;
        this.name = name;
        this.location = location;
        this.slug = slug;
        this.tags = tags;
        this.image = image;
        this.enabled = enabled;
    }

    @GeneratedValue
    @Id
    private int id;

    @Setter
    private String description;

    @Column(nullable = false)
    @Setter
    private String name;

    @ManyToOne
    @Setter
    private Location location;

    @Column(nullable = false, unique = true)
    @Setter
    @KebabCase
    private String slug;

    @ManyToMany
    @Setter
    private List<Tag> tags;

    @OneToOne
    @Setter
    private Image image;

    @Setter
    @Column(nullable = false)
    private Boolean enabled;
}
