package cz.cvut.fit.sh.reservation_system_server.entity;

import com.sun.istack.NotNull;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "T_USER")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class User {

    public User(String firstName,  String lastName,  String email, String password,
                 String username, Integer uid, List<Role> roles,  boolean active, List<Item> favourites) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.username = username;
        this.uid = uid;
        this.roles = roles;
        this.active = active;
        this.favourites = favourites;
    }

    @Id
    @GeneratedValue
    private int id;

    @NotNull
    @Setter
    @Column(nullable = false)
    private String firstName;

    @NotNull
    @Setter
    @Column(nullable = false)
    private String lastName;

    @NotNull
    @Setter
    @Column(nullable = false, unique = true)
    private String email;

    @Setter
    private String password;

    @NotNull
    @Column(unique=true, nullable = false)
    @Setter
    private String username;

    @Column(unique=true)
    @Setter
    private Integer uid;

    @ManyToMany(fetch = FetchType.EAGER)
    @Setter
    private List<Role> roles;

    @ManyToMany
    @Setter
    private List<Item> favourites;

    @NotNull
    @Setter
    @Column(nullable = false)
    private boolean active;

}
