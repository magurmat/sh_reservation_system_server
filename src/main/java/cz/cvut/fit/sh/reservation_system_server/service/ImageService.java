package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotReadImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnsupportedImageFormatException;
import cz.cvut.fit.sh.reservation_system_server.repository.ImageRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Objects;
import java.util.Optional;

@Service
public class ImageService {

    private final ImageRepository imageRepository;
    private static final Logger logger = LoggerFactory.getLogger(ImageService.class);

    @Autowired
    public ImageService(ImageRepository imageRepository) {
        this.imageRepository = imageRepository;
    }

    @Transactional
    public Image create(MultipartFile imageData) throws CannotSaveImageException, UnsupportedImageFormatException {
        if(imageData == null) {
            return null;
        }
        if(!Objects.equals(imageData.getContentType(), MediaType.IMAGE_JPEG_VALUE)
                && !Objects.equals(imageData.getContentType(), MediaType.IMAGE_PNG_VALUE)){
            throw new UnsupportedImageFormatException();
        }
        if(imageData.getOriginalFilename() == null){
            throw new CannotSaveImageException();
        }
        logger.info("Creating new image.");
        String fileName = imageData.getOriginalFilename();
        Image image = imageRepository.save(new Image(fileName.substring(fileName.lastIndexOf(".") + 1)));

        saveImageToFileSystem(image, imageData);

        return image;
    }

    @Transactional
    public void deleteImage(Image image){
        File source = new File("src/main/resources/images/" + image.getId() + "." + image.getFormat());
        source.delete();
        imageRepository.delete(image);
    }

    public Optional<Image> findById(int id){
        return imageRepository.findById(id);
    }

    public byte[] loadImageDataById(int id, String format) throws CannotReadImageException {
        try{
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            File source = new File("src/main/resources/images/" + id + "." + format);
            BufferedImage image = ImageIO.read(source) ;
            ImageIO.write(image, format, stream);
            return stream.toByteArray();
        } catch (IOException e){
            throw new CannotReadImageException();
        }
    }

    private void saveImageToFileSystem(Image image, MultipartFile imageData) throws CannotSaveImageException {
        String uploadDir = "./src/main/resources/images";

        Path uploadPath = Paths.get(uploadDir);

        try (InputStream inputStream = new ByteArrayInputStream(imageData.getBytes())) {
            if (!Files.exists(uploadPath)) {
                Files.createDirectories(uploadPath);
            }
            Path filePath = uploadPath.resolve(image.getId() + "." + image.getFormat());
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException ioe) {
            throw new CannotSaveImageException();
        }
    }
}
