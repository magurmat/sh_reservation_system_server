package cz.cvut.fit.sh.reservation_system_server.exception;

public class UnsupportedImageFormatException extends Exception{
    public UnsupportedImageFormatException ()
    {
        super("Only png and jpg format are supported!");
    }
}
