package cz.cvut.fit.sh.reservation_system_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class UnauthorizedException extends Exception{
    public UnauthorizedException (int id, String entity)
    {
        super("No authorization for " + entity + " with id:" + id);
    }

    public UnauthorizedException (String slug, String entity)
    {
        super("No authorization for " + entity + " with slug:" + slug);
    }

    public UnauthorizedException(String message) { super(message); }
    public UnauthorizedException ()
    {
        super("No authorization for this operation!");
    }
}

