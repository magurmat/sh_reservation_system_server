package cz.cvut.fit.sh.reservation_system_server.exception.handling;

import cz.cvut.fit.sh.reservation_system_server.exception.CannotReadImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.CannotSaveImageException;
import cz.cvut.fit.sh.reservation_system_server.exception.EmailAlreadyUsedException;
import cz.cvut.fit.sh.reservation_system_server.exception.IllegalLocationParentException;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemDisabledException;
import cz.cvut.fit.sh.reservation_system_server.exception.ItemOccupiedException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFavouriteItemException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundBySlugException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserAlreadyExistsException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserBannedException;
import cz.cvut.fit.sh.reservation_system_server.exception.UserNotFoundException;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.time.LocalDateTime;

@ControllerAdvice
public class ServerExceptionHandler {

    @ExceptionHandler(value = {CannotSaveImageException.class,
                                EmailAlreadyUsedException.class,
                                UserAlreadyExistsException.class,
                                IllegalLocationParentException.class,
                                ItemOccupiedException.class,
                                NotFavouriteItemException.class,
                                ItemDisabledException.class
    })
    public ResponseEntity<Object> handleBadRequestException(Exception e){
        ApiException apiException = new ApiException(
                HttpStatus.BAD_REQUEST,
                e.getMessage(),
                LocalDateTime.now()
        );

        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {
            NotFoundBySlugException.class,
            NotFoundByIdException.class,
            UserNotFoundException.class,
            CannotReadImageException.class,
            UsernameNotFoundException.class
    })
    public ResponseEntity<Object> handleNotFoundException(Exception e){
        ApiException apiException = new ApiException(
                HttpStatus.NOT_FOUND,
                e.getMessage(),
                LocalDateTime.now()
        );
        return new ResponseEntity<>(apiException, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {
            UnauthorizedException.class,
            ExpiredJwtException.class,
            UserBannedException.class,
    })
    public ResponseEntity<Object> handleNotAuthorizedException(Exception e){
        ApiException apiException = new ApiException(
                HttpStatus.FORBIDDEN,
                e.getMessage(),
                LocalDateTime.now()
        );

        return new ResponseEntity<>(apiException, HttpStatus.FORBIDDEN);
    }
    @ExceptionHandler(value = {
            DataIntegrityViolationException.class,
    })
    public ResponseEntity<Object> handlePersistenceException(DataIntegrityViolationException e){
        ApiException apiException = new ApiException(
                HttpStatus.BAD_REQUEST,
                e.getMessage(),
                LocalDateTime.now()
        );
        return new ResponseEntity<>(apiException, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {
            Exception.class,
    })
    public ResponseEntity<Object> handleUnknownException(Exception e){
        ApiException apiException = new ApiException(
                HttpStatus.INTERNAL_SERVER_ERROR,
                e.getMessage(),
                LocalDateTime.now()
        );
        return new ResponseEntity<>(apiException, HttpStatus.INTERNAL_SERVER_ERROR);
    }


}
