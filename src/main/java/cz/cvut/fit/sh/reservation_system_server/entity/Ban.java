package cz.cvut.fit.sh.reservation_system_server.entity;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "T_BAN")
@NoArgsConstructor
@Getter
@EqualsAndHashCode
@ToString
public class Ban {

    public Ban(User user, LocalDateTime expirationDate) {
        this.user = user;
        this.expirationDate = expirationDate;
    }

    @GeneratedValue
    @Id
    private int id;

    @Column(nullable = false)
    private LocalDateTime expirationDate;

    @ManyToOne(optional = false)
    private User user;

}
