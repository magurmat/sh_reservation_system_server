package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

@Data
public class ImageDTO {

    private final byte[] imageData;

    private final String format;
}
