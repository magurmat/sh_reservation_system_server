package cz.cvut.fit.sh.reservation_system_server.exception;

public class IllegalLocationParentException extends Exception{
    public IllegalLocationParentException ()
    {
        super("Location cannot be child location of itself.");
    }
}
