package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserCreateDTO {

    private String firstName;

    private String lastName;

    private String email;

    private String password;

    private String username;

    private Integer uid;

    private List<Integer> roleIds;

    private boolean active;

    private List<Integer> favouriteIds;
}
