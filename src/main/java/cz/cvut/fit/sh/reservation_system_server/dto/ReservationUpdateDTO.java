package cz.cvut.fit.sh.reservation_system_server.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReservationUpdateDTO {

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime startTime;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern="yyyy-MM-dd HH:mm:ss")
    private LocalDateTime endTime;

    private ReservationState state;

    private int userId;

    private int itemId;
}
