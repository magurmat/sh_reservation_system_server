package cz.cvut.fit.sh.reservation_system_server.repository;


import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Integer> {

    List<Reservation> findAllByUserId(int id);

    List<Reservation> findAllByItemId(int id);
}
