package cz.cvut.fit.sh.reservation_system_server.dto;

import lombok.Data;

@Data
public class TagDTO {
    private final int id;

    private final String name;
}
