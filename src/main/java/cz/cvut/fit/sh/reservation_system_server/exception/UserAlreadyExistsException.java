package cz.cvut.fit.sh.reservation_system_server.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class UserAlreadyExistsException extends Exception{
    public UserAlreadyExistsException (String username)
    {
        super("User with name " + username + " already exists!");
    }
}
