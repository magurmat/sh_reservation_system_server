package cz.cvut.fit.sh.reservation_system_server.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KebabCaseValidator
        implements ConstraintValidator<KebabCase, String> {

    private Pattern pattern;
    private Matcher matcher;
    private static final String kebab_pattern = "^([a-z][a-z0-9]*)(-[a-z0-9]+)*$";
    @Override
    public void initialize(KebabCase constraintAnnotation) {
    }
    @Override
    public boolean isValid(String string, ConstraintValidatorContext context){
        return (validate(string));
    }
    private boolean validate(String string) {
        pattern = Pattern.compile(kebab_pattern);
        matcher = pattern.matcher(string);
        return matcher.matches();
    }
}
