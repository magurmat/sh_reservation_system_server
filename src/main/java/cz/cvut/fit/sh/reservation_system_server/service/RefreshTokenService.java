package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.entity.RefreshToken;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.ExpiredRefreshTokenException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.RefreshTokenRepository;
import cz.cvut.fit.sh.reservation_system_server.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class RefreshTokenService {

    private final UserRepository userRepository;
    private final RefreshTokenRepository refreshTokenRepository;
    private final long refreshExpirationDateInMs = 3600L * 24 * 30;

    @Autowired

    public RefreshTokenService(UserRepository userRepository, RefreshTokenRepository refreshTokenRepository) {
        this.userRepository = userRepository;
        this.refreshTokenRepository = refreshTokenRepository;
    }

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(Integer userId) throws NotFoundByIdException {
        RefreshToken refreshToken = new RefreshToken();
        Optional<User> user = userRepository.findById(userId);
        if(user.isEmpty()){
            throw new NotFoundByIdException(userId, "user");
        }
        refreshToken.setUser(user.get());
        refreshToken.setExpiryDate(LocalDateTime.now().plusSeconds(refreshExpirationDateInMs));
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) throws ExpiredRefreshTokenException {
        if (token.getExpiryDate().compareTo(LocalDateTime.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new ExpiredRefreshTokenException();
        }

        return token;
    }

    public List<RefreshToken> findByUser(User user) {
        return refreshTokenRepository.findByUser(user);
    }

    @Transactional
    public void deleteByUserId(Integer userId) {
        Optional<User> user = userRepository.findById(userId);
        user.ifPresent(refreshTokenRepository::deleteByUser);
    }

    @Transactional
    public void delete(Integer id) throws NotFoundByIdException {
        Optional<RefreshToken> token = refreshTokenRepository.findById(id);
        if (token.isEmpty()) {
            throw new NotFoundByIdException(id, "Refresh token");
        }
        refreshTokenRepository.delete(token.get());
    }
}
