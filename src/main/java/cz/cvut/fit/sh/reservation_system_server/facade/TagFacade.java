package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.TagCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.TagDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.TagUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Component
public class TagFacade {

    private final TagService tagService;
    private final RoleService roleService;
    private final ItemService itemService;
    private final SecurityManager securityManager;


    @Autowired
    TagFacade(TagService tagService, RoleService roleService, ItemService itemService, SecurityManager securityManager) {
        this.tagService = tagService;
        this.roleService = roleService;
        this.itemService = itemService;
        this.securityManager = securityManager;
    }

    public List<TagDTO> findAll() throws UnauthorizedException {
        if(securityManager.getAllTagsAccess(SecurityContextHolder.getContext().getAuthentication())){
            return tagService.findAll().stream().map(this::toDTO).toList();
        }
        throw new UnauthorizedException("No access to roles!");

    }

    public List<TagDTO> findByIds(List<Integer> ids) throws UnauthorizedException {
        for(Integer tagId : ids){
            if(!securityManager.getTagAccess(tagId, SecurityContextHolder.getContext().getAuthentication())){
                throw new UnauthorizedException(tagId,"tag");
            }
        }
        return tagService.findByIds(ids).stream().map(this::toDTO).toList();
    }

    public TagDTO findById(int id) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.getTagAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(tagService.findById(id)).orElseThrow(() ->new NotFoundByIdException(id,"Tag"));
        }
        throw new UnauthorizedException(id,"tag");
    }

    public TagDTO create(TagCreateDTO tagCreateDTO) throws NotFoundByIdException, UnauthorizedException {
        if(securityManager.createTagAccess(SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(tagService.create(tagCreateDTO));
        }
        throw new UnauthorizedException();

    }

    public TagDTO update(int id, TagUpdateDTO tagDTO) throws NotFoundByIdException, UnauthorizedException {
        Optional<Tag> optionalTag = tagService.findById(id);
        if (optionalTag.isEmpty())
            throw new NotFoundByIdException(id, "Tag");

        if(securityManager.updateTagAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            return toDTO(tagService.update(id, tagDTO));
        }
        throw new UnauthorizedException(id,"tag");

    }

    @Transactional
    public void deleteById(int id) throws NotFoundByIdException, UnauthorizedException {
        Optional<Tag> optionalTag = tagService.findById(id);
        if(optionalTag.isEmpty()) {
            throw new NotFoundByIdException(id, "Tag");
        }
        if(securityManager.deleteTagAccess(id, SecurityContextHolder.getContext().getAuthentication())){
            roleService.removeManagedTag(optionalTag.get());
            itemService.removeTag(optionalTag.get());
            tagService.deleteById(id);
        }
        throw new UnauthorizedException(id,"tag");


    }

    private TagDTO toDTO(Tag tag) {
        return new TagDTO(tag.getId(), tag.getName());
    }

    private Optional<TagDTO> toDTO(Optional<Tag> tag) {
        if (tag.isEmpty())
            return Optional.empty();
        return Optional.of(toDTO(tag.get()));
    }
}
