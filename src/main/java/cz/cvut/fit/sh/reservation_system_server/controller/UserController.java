package cz.cvut.fit.sh.reservation_system_server.controller;

import cz.cvut.fit.sh.reservation_system_server.dto.BanCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.UserUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFavouriteItemException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.facade.UserFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@CrossOrigin("*")
@RequestMapping("/user")
public class UserController {

    private final UserFacade userFacade;

    @Autowired
    public UserController(UserFacade userFacade) {
        this.userFacade = userFacade;
    }

    @GetMapping
    ResponseEntity<List<UserDTO>> allUsers() throws UnauthorizedException {
        return new ResponseEntity<>(userFacade.findAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<UserDTO> findUserById(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(userFacade.findById(id), HttpStatus.OK);
    }

    @PostMapping("/{userId}/favourite/{itemId}")
    @ResponseStatus(HttpStatus.CREATED)
    ResponseEntity<UserDTO> addFavouriteItem(@PathVariable int userId, @PathVariable int itemId) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(userFacade.addFavourite(userId, itemId), HttpStatus.OK);
    }

    @DeleteMapping("/{userId}/favourite/{itemId}")
    ResponseEntity<UserDTO> deleteFavouriteItem(@PathVariable int userId, @PathVariable int itemId) throws UnauthorizedException, NotFavouriteItemException, NotFoundByIdException {
        return new ResponseEntity<>(userFacade.removeFavourite(userId, itemId), HttpStatus.OK);
    }

    @PutMapping("/{id}")
    ResponseEntity<UserDTO> updateUser(@PathVariable int id, @RequestBody UserUpdateDTO user) throws NotFoundByIdException, UnauthorizedException {
        return new ResponseEntity<>(userFacade.update(id, user), HttpStatus.OK);
    }

    @PostMapping("/ban")
    ResponseEntity<?> banUser(@RequestBody BanCreateDTO ban) throws NotFoundByIdException, UnauthorizedException {
        userFacade.banUser(ban);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> deleteUser(@PathVariable int id) throws NotFoundByIdException, UnauthorizedException {
        userFacade.deleteById(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }


}

