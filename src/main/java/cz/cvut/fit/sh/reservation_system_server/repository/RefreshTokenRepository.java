package cz.cvut.fit.sh.reservation_system_server.repository;

import cz.cvut.fit.sh.reservation_system_server.entity.RefreshToken;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;

import java.util.List;
import java.util.Optional;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, Integer> {

    Optional<RefreshToken> findByToken(String token);

    List<RefreshToken> findByUser(User user);

    @Modifying
    void deleteByUser(User user);
}
