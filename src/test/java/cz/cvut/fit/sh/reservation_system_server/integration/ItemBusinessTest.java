package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.facade.ItemFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.ItemRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {ItemFacade.class, ItemService.class})
public class ItemBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(ItemBusinessTest.class);

    @Autowired
    private ItemFacade itemFacade;
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Tag tag = new Tag("tag");

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private LocationService locationService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private UserService userService;

    @MockBean
    private TagService tagService;

    @MockBean
    private ImageService imageService;
    Item item1 = new Item("desc", "name", location1, "slug1", List.of(tag), new Image(), true);
    Item item2 = new Item("desc", "name", location1, "slug2", new ArrayList<>(), new Image(), true);
    Item item3 = new Item("desc", "name", location1, "slug3", new ArrayList<>(), new Image(), true);
    Item item4 = new Item("desc", "name", location1, "slug4", List.of(tag), new Image(), true);
    List<Item> items = List.of(item1, item2, item3, item4);
    @Autowired
    private ItemService itemService;
    @MockBean
    private ItemRepository itemRepository;
    @MockBean
    private SecurityManager securityManager;

    @Test
    void getItems() throws Exception {

        BDDMockito.given(itemRepository.findAll()).willReturn(items);
        BDDMockito.given(securityManager.getAllItemsAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (itemFacade.findAll().size() == items.size());
    }

    @Test
    void getItem() throws Exception {

        BDDMockito.given(itemRepository.findById(item1.getId())).willReturn(Optional.ofNullable(item1));
        BDDMockito.given(securityManager.getItemAccess(item1.getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(itemFacade.findById(item1.getId()), toDTO(item1)));
    }

    private ItemDTO toDTO(Item item) {
        return new ItemDTO(item.getId(), item.getDescription(), item.getName(), item.getLocation().getId(), item.getSlug(),
                item.getTags().stream().map(Tag::getId).toList(), item.getImage().getId(), item.getEnabled());
    }
}
