package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.entity.RefreshToken;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.repository.RefreshTokenRepository;
import cz.cvut.fit.sh.reservation_system_server.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest(classes = RefreshTokenService.class)
public class RefreshTokenServiceTest {

    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, new ArrayList<>(), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, new ArrayList<>(), true, new ArrayList<>());
    RefreshToken refreshToken1 = new RefreshToken(user1, UUID.randomUUID().toString(), LocalDateTime.now().plusDays(7));
    RefreshToken refreshToken2 = new RefreshToken(user1, UUID.randomUUID().toString(), LocalDateTime.now().plusDays(777));
    RefreshToken refreshToken3 = new RefreshToken(user2, UUID.randomUUID().toString(), LocalDateTime.now().plusDays(7));
    RefreshToken refreshToken4 = new RefreshToken(user2, UUID.randomUUID().toString(), LocalDateTime.now().plusDays(777));
    @Autowired
    private RefreshTokenService refreshTokenService;
    @MockBean
    private RefreshTokenRepository refreshTokenRepository;
    @MockBean
    private UserRepository userRepository;

    @Test
    void findById() {

        BDDMockito.given(refreshTokenRepository.findByToken(refreshToken1.getToken())).willReturn(Optional.of(refreshToken1));

        Assertions.assertEquals(Optional.of(refreshToken1), refreshTokenService.findByToken(refreshToken1.getToken()));

        Mockito.verify(refreshTokenRepository, Mockito.atLeastOnce()).findByToken(refreshToken1.getToken());
    }

}