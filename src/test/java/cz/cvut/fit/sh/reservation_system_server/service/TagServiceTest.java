package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.repository.TagRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = TagService.class)
public class TagServiceTest {

    Tag tag1 = new Tag("tag1");
    Tag tag2 = new Tag("tag2");
    Tag tag3 = new Tag("tag3");
    Tag tag4 = new Tag("tag4");
    @Autowired
    private TagService tagService;
    @MockBean
    private TagRepository tagRepository;

    @Test
    void findAll() {
        List<Tag> tags = Arrays.asList(tag1, tag2, tag3, tag4);

        BDDMockito.given(tagRepository.findAll()).willReturn(tags);
        Assertions.assertEquals(tags, tagService.findAll());
        Mockito.verify(tagRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() {
        List<Tag> tags = Arrays.asList(tag1, tag2);
        List<Integer> tagIds = tags.stream().map(Tag::getId).toList();

        BDDMockito.given(tagRepository.findAllById(tagIds)).willReturn(tags);
        Assertions.assertEquals(tags, tagService.findByIds(tagIds));
        Mockito.verify(tagRepository, Mockito.atLeastOnce()).findAllById(tagIds);
    }

    @Test
    void findById() {

        BDDMockito.given(tagRepository.findById(tag1.getId())).willReturn(Optional.of(tag1));

        Assertions.assertEquals(Optional.of(tag1), tagService.findById(tag1.getId()));

        Mockito.verify(tagRepository, Mockito.atLeastOnce()).findById(tag1.getId());
    }


}
