package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.facade.RoleFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.RoleRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {RoleFacade.class, RoleService.class})
public class RoleBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(RoleBusinessTest.class);
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(null, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location3 = new Location(location1, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    Item item4 = new Item("desc", "name", location4, "slug", new ArrayList<>(), new Image(), true);
    Role role3 = new Role(List.of(item4), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    Item item3 = new Item("desc", "name", location3, "slug", new ArrayList<>(), new Image(), true);
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Role role2 = new Role(List.of(item1, item3), new ArrayList<>(), List.of(location3), new ArrayList<>(), "a", true);
    Item item2 = new Item("desc", "name", location2, "slug", new ArrayList<>(), new Image(), true);
    Role role1 = new Role(List.of(item1, item2), new ArrayList<>(), List.of(location1), new ArrayList<>(), "a", true);
    Role role4 = new Role(List.of(item1, item2, item3, item4), List.of(role1, role2, role3),
            List.of(location1, location2, location3, location4), new ArrayList<>(), "a", true);
    List<Role> roles = List.of(role1, role2, role3, role4);
    @Autowired
    private RoleService roleService;
    @Autowired
    private RoleFacade roleFacade;
    @MockBean
    private ItemService itemService;
    @MockBean
    private LocationService locationService;
    @MockBean
    private UserService userService;
    @MockBean
    private TagService tagService;
    @MockBean
    private SecurityManager securityManager;
    @MockBean
    private RoleRepository roleRepository;

    @Test
    void getRoles() throws Exception {

        BDDMockito.given(roleRepository.findAll()).willReturn(roles);
        BDDMockito.given(securityManager.getAllRolesAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (roleFacade.findAll().size() == roles.size());
    }

    @Test
    void getRole() throws Exception {

        BDDMockito.given(roleRepository.findById(role1.getId())).willReturn(Optional.ofNullable(role1));
        BDDMockito.given(securityManager.getRoleAccess(role1.getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(roleFacade.findById(role1.getId()), toDTO(role1)));
    }

    private RoleDTO toDTO(Role role) {
        return new RoleDTO(role.getId(),
                role.getName(),
                role.getManagedItems().stream().map(Item::getId).toList(),
                role.getManagedRoles().stream().map(Role::getId).toList(),
                role.getManagedLocations().stream().map(Location::getId).toList(),
                role.getManagedTags().stream().map(Tag::getId).toList(),
                role.isAdmin());
    }
}
