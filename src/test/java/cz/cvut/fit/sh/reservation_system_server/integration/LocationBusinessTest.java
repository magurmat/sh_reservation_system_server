package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.LocationDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.facade.LocationFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.LocationRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {LocationFacade.class, LocationService.class})
public class LocationBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(LocationBusinessTest.class);
    @MockBean
    LocationRepository locationRepository;
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(location1, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location3 = new Location(location2, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    List<Location> locations = List.of(location1, location2, location3, location4);
    @Autowired
    private LocationService locationService;
    @Autowired
    private LocationFacade locationFacade;
    @MockBean
    private ItemService itemService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private ReservationService reservationService;
    @MockBean
    private ImageService imageService;
    @MockBean
    private SecurityManager securityManager;

    @Test
    void getLocations() throws Exception {

        BDDMockito.given(locationRepository.findAll()).willReturn(locations);
        BDDMockito.given(securityManager.getAllLocationsAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (locationFacade.findAll().size() == locations.size());
    }

    @Test
    void getLocation() throws Exception {

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.ofNullable(location1));
        BDDMockito.given(securityManager.getLocationAccess(location1.getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(locationFacade.findById(location1.getId()), toDTO(location1)));
    }

    private LocationDTO toDTO(Location location) {
        Integer parentId = null;
        if (location.getParentLocation() != null) {
            parentId = location.getParentLocation().getId();
        }
        return new LocationDTO(location.getId(),
                parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                location.getImage().getId());
    }
}
