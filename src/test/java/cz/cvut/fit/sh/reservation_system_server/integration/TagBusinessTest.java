package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.TagDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.facade.TagFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.TagRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {TagFacade.class, TagService.class})
public class TagBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(TagBusinessTest.class);
    Tag tag1 = new Tag("tagName1");
    Tag tag2 = new Tag("tagName2");
    Tag tag3 = new Tag("tagName3");
    Tag tag4 = new Tag("tagName4");
    List<Tag> tags = List.of(tag1, tag2, tag3, tag4);
    @Autowired
    private TagService tagService;
    @Autowired
    private TagFacade tagFacade;
    @MockBean
    private RoleService roleService;
    @MockBean
    private ItemService itemService;
    @MockBean
    private SecurityManager securityManager;
    @MockBean
    private TagRepository tagRepository;

    @Test
    void getTags() throws Exception {

        BDDMockito.given(tagRepository.findAll()).willReturn(tags);
        BDDMockito.given(securityManager.getAllTagsAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (tagFacade.findAll().size() == tags.size());
    }

    @Test
    void getTag() throws Exception {

        BDDMockito.given(tagRepository.findById(tag1.getId())).willReturn(Optional.ofNullable(tag1));
        BDDMockito.given(securityManager.getTagAccess(tag1.getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(tagFacade.findById(tag1.getId()), toDTO(tag1)));
    }

    private TagDTO toDTO(Tag tag) {
        return new TagDTO(tag.getId(),
                tag.getName());
    }
}
