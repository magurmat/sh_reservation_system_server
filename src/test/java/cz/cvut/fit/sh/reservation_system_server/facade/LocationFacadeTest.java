package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.LocationDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = LocationFacade.class)
public class LocationFacadeTest {

    @Autowired
    LocationFacade locationFacade;

    @MockBean
    private LocationService locationService;

    @MockBean
    private ItemService itemService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private ImageService imageService;

    @MockBean
    private SecurityManager securityManager;

    @MockBean
    private Authentication auth;

    @MockBean
    private SecurityContext context;

    List<Location> location1ChildList = new ArrayList<>();
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(location1, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location3 = new Location(location2, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    Location location5 = new Location(location4,
            "locationName5", "locationDescr5", "locationSlug5", new Image());
    Location location6 = new Location(location5,
            "locationName6", "locationDescr6", "locationSlug6", new Image());
    Location location7 = new Location(location6,
            "locationName7", "locationDescr7", "locationSlug7", new Image());

    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<Location> locations = Arrays.asList(location1, location2, location3, location4);
        List<LocationDTO> locationsDTO = locations.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllLocationsAccess(auth)).willReturn(true);
        BDDMockito.given(locationService.findAll()).willReturn(locations);
        Assertions.assertEquals(locationsDTO, locationFacade.findAll());
        Mockito.verify(locationService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException {
        List<Location> locations = Arrays.asList(location1, location2);
        List<Integer> locationIds = locations.stream().map(Location::getId).toList();

        for (Integer id : locationIds) {
            BDDMockito.given(securityManager.getLocationAccess(id, auth)).willReturn(true);
        }

        BDDMockito.given(locationService.findByIds(locationIds)).willReturn(locations);
        Assertions.assertEquals(locations.stream().map(this::toDTO).collect(Collectors.toList()), locationFacade.findByIds(locationIds));
        Mockito.verify(locationService, Mockito.atLeastOnce()).findByIds(locationIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(locationService.findById(location1.getId())).willReturn(Optional.of(location1));
        BDDMockito.given(securityManager.getLocationAccess(location1.getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(location1), locationFacade.findById(location1.getId()));

        Mockito.verify(locationService, Mockito.atLeastOnce()).findById(location1.getId());
    }

    private LocationDTO toDTO(Location location) {
        Integer parentId = null;
        if(location.getParentLocation() != null){
            parentId = location.getParentLocation().getId();
        }
        return new LocationDTO(location.getId(),
                parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                location.getImage().getId());
    }

}
