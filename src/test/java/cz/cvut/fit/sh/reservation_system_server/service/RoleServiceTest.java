package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.repository.RoleRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = RoleService.class)
public class RoleServiceTest {

    @Autowired
    private RoleService roleService;

    @MockBean
    private RoleRepository roleRepository;

    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(location1, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location3 = new Location(location2, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item2 = new Item("desc", "name", location2, "slug", new ArrayList<>(), new Image(), true);
    Item item3 = new Item("desc", "name", location3, "slug", new ArrayList<>(), new Image(), true);
    Item item4 = new Item("desc", "name", location4, "slug", new ArrayList<>(), new Image(), true);
    Role role1 = new Role(List.of(item1, item2), new ArrayList<>(), List.of(location1), new ArrayList<>(), "a" ,true);
    Role role2 = new Role(List.of(item1, item3), new ArrayList<>(), List.of(location3), new ArrayList<>(), "a" ,true);
    Role role3 = new Role(List.of(item4), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a" ,true);
    Role role4 = new Role(List.of(item1, item2, item3, item4), List.of(role1, role2, role3),
            List.of(location1, location2, location3, location4), new ArrayList<>(), "a" ,true);

    @Test
    void findAll(){
        List<Role> roles= Arrays.asList(role1, role2, role3, role4);

        BDDMockito.given(roleRepository.findAll()).willReturn(roles);
        Assertions.assertEquals(roles, roleService.findAll());
        Mockito.verify(roleRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds(){
        List<Role> roles= Arrays.asList(role1, role2);
        List<Integer> roleIds= roles.stream().map(Role::getId).toList();

        BDDMockito.given(roleRepository.findAllById(roleIds)).willReturn(roles);
        Assertions.assertEquals(roles, roleService.findByIds(roleIds));
        Mockito.verify(roleRepository,Mockito.atLeastOnce()).findAllById(roleIds);
    }

    @Test
    void findById() {

        BDDMockito.given(roleRepository.findById(role1.getId())).willReturn(Optional.of(role1));

        Assertions.assertEquals(Optional.of(role1), roleService.findById(role1.getId()));

        Mockito.verify(roleRepository, Mockito.atLeastOnce()).findById(role1.getId());
    }

    @Test
    void findParentRoles() {

        BDDMockito.given(roleRepository.findAllByManagedRolesIn(List.of(role1))).willReturn(List.of(role4));

        Assertions.assertEquals(List.of(role4), roleService.findParentRoles(role1));

        Mockito.verify(roleRepository, Mockito.atLeastOnce()).findAllByManagedRolesIn(List.of(role1));
    }

    private RoleDTO toDTO(Role role) {
        return new RoleDTO(role.getId(),
                role.getName(),
                role.getManagedItems().stream().map(Item::getId).toList(),
                role.getManagedRoles().stream().map(Role::getId).toList(),
                role.getManagedLocations().stream().map(Location::getId).toList(),
                role.getManagedTags().stream().map(Tag::getId).toList(),
                role.isAdmin());
    }
}

