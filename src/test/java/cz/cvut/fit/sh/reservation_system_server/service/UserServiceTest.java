package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.UserDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@SpringBootTest(classes = UserService.class)
public class UserServiceTest {

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository userRepository;

    Role role1 = new Role(new ArrayList<>() , new ArrayList<>(), new ArrayList<>(), new ArrayList<>(),"a", true);
    Role role2 = new Role(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(),"a", true);
    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, List.of(role1), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, List.of(role1), true, new ArrayList<>());
    User user3 = new User("firstname", "lastname", "email", "password",
            "username3", 3, List.of(role1), true, new ArrayList<>());
    User user4 = new User("firstname", "lastname", "email", "password",
            "username4", 4, List.of(role1, role2), true, new ArrayList<>());

    @Test
    void findAll(){
        List<User> users= Arrays.asList(user1, user2, user3, user4);

        BDDMockito.given(userRepository.findAll()).willReturn(users);
        Assertions.assertEquals(users, userService.findAll());
        Mockito.verify(userRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds(){
        List<User> users= Arrays.asList(user1, user2);
        List<Integer> userIds= users.stream().map(User::getId).toList();

        BDDMockito.given(userRepository.findAllById(userIds)).willReturn(users);
        Assertions.assertEquals(users, userService.findByIds(userIds));
        Mockito.verify(userRepository,Mockito.atLeastOnce()).findAllById(userIds);
    }

    @Test
    void findById() {

        BDDMockito.given(userRepository.findById(user1.getId())).willReturn(Optional.of(user1));

        Assertions.assertEquals(Optional.of(user1), userService.findById(user1.getId()));

        Mockito.verify(userRepository, Mockito.atLeastOnce()).findById(user1.getId());
    }

    @Test
    void findByRole() {

        BDDMockito.given(userRepository.findAllByRolesIn(Set.of(role2))).willReturn(List.of(user4));

        Assertions.assertEquals(List.of(user4), userService.findByRole(role2));

        Mockito.verify(userRepository, Mockito.atLeastOnce()).findAllByRolesIn(Set.of(role2));
    }

    @Test
    void findByUsername() {

        BDDMockito.given(userRepository.findUserByUsername(user1.getUsername())).willReturn(Optional.of(user1));

        Assertions.assertEquals(Optional.of(user1), userService.findByUsername(user1.getUsername()));

        Mockito.verify(userRepository, Mockito.atLeastOnce()).findUserByUsername(user1.getUsername());
    }

    @Test
    void findByEmail() {

        BDDMockito.given(userRepository.findUserByEmail(user1.getEmail())).willReturn(Optional.of(user1));

        Assertions.assertEquals(Optional.of(user1), userService.findByEmail(user1.getEmail()));

        Mockito.verify(userRepository, Mockito.atLeastOnce()).findUserByEmail(user1.getEmail());
    }

    private UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getPassword(), user.getUsername(), user.getUid(),
                user.getRoles().stream().map(Role::getId).toList(),
                user.getFavourites().stream().map(Item::getId).toList(),
                user.isActive());
    }
}


