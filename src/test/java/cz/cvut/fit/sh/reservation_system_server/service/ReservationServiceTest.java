package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.repository.ReservationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = ReservationService.class)
public class ReservationServiceTest {

    @Autowired
    private ReservationService reservationService;

    @MockBean
    private ReservationRepository reservationRepository;

    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, new ArrayList<>(), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, new ArrayList<>(), true, new ArrayList<>());
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item2 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Reservation reservation1 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item1);
    Reservation reservation2 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item1);
    Reservation reservation3 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item2);
    Reservation reservation4 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item2);


    @Test
    void findAll(){
        List<Reservation> reservations= Arrays.asList(reservation1, reservation2, reservation3, reservation4);

        BDDMockito.given(reservationRepository.findAll()).willReturn(reservations);
        Assertions.assertEquals(reservations, reservationService.findAll());
        Mockito.verify(reservationRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds(){
        List<Reservation> reservations= Arrays.asList(reservation1, reservation2);
        List<Integer> reservationIds= reservations.stream().map(Reservation::getId).toList();

        BDDMockito.given(reservationRepository.findAllById(reservationIds)).willReturn(reservations);
        Assertions.assertEquals(reservations, reservationService.findByIds(reservationIds));
        Mockito.verify(reservationRepository,Mockito.atLeastOnce()).findAllById(reservationIds);
    }

    @Test
    void findById() {

        BDDMockito.given(reservationRepository.findById(reservation1.getId())).willReturn(Optional.of(reservation1));

        Assertions.assertEquals(Optional.of(reservation1), reservationService.findById(reservation1.getId()));

        Mockito.verify(reservationRepository, Mockito.atLeastOnce()).findById(reservation1.getId());
    }

    @Test
    void findByUserId() {

        BDDMockito.given(reservationRepository.findAllByUserId(user1.getId())).willReturn(List.of(reservation1, reservation3));

        Assertions.assertEquals(List.of(reservation1, reservation3), reservationService.findByUserId(user1.getId()));

        Mockito.verify(reservationRepository, Mockito.atLeastOnce()).findAllByUserId(user1.getId());
    }

    @Test
    void findByItemId() {

        BDDMockito.given(reservationRepository.findAllByItemId(item1.getId())).willReturn(List.of(reservation1, reservation2));

        Assertions.assertEquals(List.of(reservation1, reservation2), reservationService.findByItemId(item1.getId()));

        Mockito.verify(reservationRepository, Mockito.atLeastOnce()).findAllByItemId(item1.getId());
    }


    private ReservationDTO toDTO(Reservation reservation) {
        return new ReservationDTO(reservation.getId(),
                reservation.getStartTime(),
                reservation.getEndTime(),
                reservation.getState(),
                reservation.getUser().getId(),
                reservation.getUser().getFirstName(),
                reservation.getUser().getLastName(),
                reservation.getItem().getId());
    }
}
