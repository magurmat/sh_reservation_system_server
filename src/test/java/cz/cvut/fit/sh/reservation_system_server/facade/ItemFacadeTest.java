package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ImageService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = ItemFacade.class)
public class ItemFacadeTest {

    @Autowired
    private ItemFacade itemFacade;

    @MockBean
    private ItemService itemService;

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private LocationService locationService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private SecurityManager securityManager;

    @MockBean
    private UserService userService;

    @MockBean
    private TagService tagService;

    @MockBean
    private ImageService imageService;

    @MockBean
    private Authentication auth;

    @MockBean
    private SecurityContext context;

    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item2 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item3 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item4 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);

    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<Item> items = Arrays.asList(item1, item2, item3, item4);
        List<ItemDTO> itemsDTO = items.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllItemsAccess(auth)).willReturn(true);
        BDDMockito.given(itemService.findAll()).willReturn(items);
        Assertions.assertEquals(itemsDTO, itemFacade.findAll());
        Mockito.verify(itemService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException {
        List<Item> items = Arrays.asList(item1, item2);
        List<Integer> itemIds = items.stream().map(Item::getId).toList();

        for (Integer id : itemIds) {
            BDDMockito.given(securityManager.getItemAccess(id, auth)).willReturn(true);
        }

        BDDMockito.given(itemService.findByIds(itemIds)).willReturn(items);
        Assertions.assertEquals(items.stream().map(this::toDTO).collect(Collectors.toList()), itemFacade.findByIds(itemIds));
        Mockito.verify(itemService, Mockito.atLeastOnce()).findByIds(itemIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(itemService.findById(item1.getId())).willReturn(Optional.of(item1));
        BDDMockito.given(securityManager.getItemAccess(item1.getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(item1), itemFacade.findById(item1.getId()));

        Mockito.verify(itemService, Mockito.atLeastOnce()).findById(item1.getId());
    }

    private ItemDTO toDTO(Item item) {
        return new ItemDTO(item.getId(), item.getDescription(), item.getName(), item.getLocation().getId(), item.getSlug(),
                item.getTags().stream().map(Tag::getId).toList(), item.getImage().getId(), item.getEnabled());
    }
}
