package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.LocationCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.LocationUpdateDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.exception.IllegalLocationParentException;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.repository.LocationRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = LocationService.class)
public class LocationServiceTest {
    @Autowired
    private LocationService locationService;

    @MockBean
    private LocationRepository locationRepository;

    MultipartFile multipartFile = new MockMultipartFile("file",
            "filename", "image/png", new byte [10]);
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(null, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location8 = new Location(location2, "locationName8", "locationDescr8", "locationSlug8", new Image());
    Location location3 = new Location(location1, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    Location location5 = new Location(location4,
                                                "locationName5", "locationDescr5", "locationSlug5", new Image());
    Location location6 = new Location(location3,
            "locationName6", "locationDescr6", "locationSlug6", new Image());
    Location location7 = new Location(location5,
            "locationName7", "locationDescr7", "locationSlug7", new Image());

    @Test
    void findAll(){
        List<Location> locations= Arrays.asList(location1, location2, location3, location4, location5, location6, location7);

        BDDMockito.given(locationRepository.findAll()).willReturn(locations);
        Assertions.assertEquals(locations, locationService.findAll());
        Mockito.verify(locationRepository,Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds(){
        List<Location> locations= Arrays.asList(location1, location2, location3, location4);
        List<Integer> locationsIds= locations.stream().map(Location::getId).toList();

        BDDMockito.given(locationRepository.findAllById(locationsIds)).willReturn(locations);
        Assertions.assertEquals(locations, locationRepository.findAllById(locationsIds));
        Mockito.verify(locationRepository,Mockito.atLeastOnce()).findAllById(locationsIds);
    }

    @Test
    void findById() {

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.of(location1));

        Assertions.assertEquals(Optional.of(location1), locationService.findById(location1.getId()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location1.getId());
    }

    @Test
    void findBySlug() {

        BDDMockito.given(locationRepository.findBySlug(location1.getSlug())).willReturn(Optional.of(location1));

        Assertions.assertEquals(Optional.of(location1), locationService.findBySlug(location1.getSlug()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findBySlug(location1.getSlug());
    }

    @Test
    void createWithParentLocation() throws NotFoundByIdException {
        LocationCreateDTO locationCreateDTO = toCreateDTO(location3);

        BDDMockito.given(locationRepository.findById(locationCreateDTO.getParentLocationId())).willReturn(Optional.of(location1));
        BDDMockito.given(locationRepository.save(new Location(location1,
                "locationName3",
                "locationDescr3", "locationSlug3", new Image()))).willReturn(new Location(location1,
                "locationName3",
               "locationDescr3", "locationSlug3", new Image()));

        Assertions.assertEquals(location3, locationService.create(locationCreateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(location1,
                "locationName3",
               "locationDescr3", "locationSlug3", new Image()));
    }

    @Test
    void createWithoutParentLocation() throws NotFoundByIdException {
        LocationCreateDTO locationCreateDTO = toCreateDTO(location1);

        BDDMockito.given(locationRepository.save(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()))).willReturn(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()));

        Assertions.assertEquals(location1, locationService.create(locationCreateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()));
    }

    @Test
    void createNonExistentParentLocation() {
        LocationCreateDTO locationCreateDTO = toCreateDTO(location3);
        locationCreateDTO.setParentLocationId(455);

        BDDMockito.given(locationRepository.findById(locationCreateDTO.getParentLocationId())).willReturn(Optional.empty());

        Assertions.assertThrows(NotFoundByIdException.class,
                ()-> locationService.create(locationCreateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(locationCreateDTO.getParentLocationId());
    }

    @Test
    void updateWithParentLocation() throws Exception {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location8);
        locationUpdateDTO.setParentLocationId(2);

        BDDMockito.given(locationRepository.findById(location3.getId())).willReturn(Optional.of(location3));
        BDDMockito.given(locationRepository.findById(2)).willReturn(Optional.of(location2));
        BDDMockito.given(locationRepository.save(new Location(location2,
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()))).willReturn(new Location(location2,
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()));

        Assertions.assertEquals(location8, locationService.update(location3.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(location2,
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()));
    }

    @Test
    void updateWithoutParentLocation() throws Exception {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location2);

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.of(location1));
        BDDMockito.given(locationRepository.save(new Location(null,
                "locationName2",
                "locationDescr2", "locationSlug2", new Image()))).willReturn(new Location(null,
                "locationName2",
                "locationDescr2", "locationSlug2", new Image()));

        Assertions.assertEquals(location2, locationService.update(location1.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(null,
                "locationName2",
                "locationDescr2", "locationSlug2", new Image()));
    }

    @Test
    void updateAddParentLocation() throws Exception {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location8);
        locationUpdateDTO.setParentLocationId(2);

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.of(location1));
        BDDMockito.given(locationRepository.findById(2)).willReturn(Optional.of(location2));
        BDDMockito.given(locationRepository.save(new Location(location8.getParentLocation(),
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()))).willReturn(new Location(location8.getParentLocation(),
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()));

        Assertions.assertEquals(location8, locationService.update(location1.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(location8.getParentLocation(),
                "locationName8",
                "locationDescr8", "locationSlug8", new Image()));
    }

    @Test
    void updateRemoveParentLocation() throws Exception {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location1);

        BDDMockito.given(locationRepository.findById(location8.getId())).willReturn(Optional.of(location8));
        BDDMockito.given(locationRepository.save(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()))).willReturn(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()));

        Assertions.assertEquals(location1, locationService.update(location8.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).save(new Location(null,
                "locationName1",
                "locationDescr1", "locationSlug1", new Image()));
    }

    @Test
    void updateNonexistentParentLocation() {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location1);
        locationUpdateDTO.setParentLocationId(455);

        BDDMockito.given(locationRepository.findById(location8.getId())).willReturn(Optional.of(location8));

        Assertions.assertThrows(NotFoundByIdException.class,
                ()-> locationService.update(location8.getId() ,locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location8.getId());
    }

    @Test
    void updateParentItselfLocation() {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location8);
        locationUpdateDTO.setParentLocationId(location8.getId());

        BDDMockito.given(locationRepository.findById(location8.getId())).willReturn(Optional.of(location8));
        BDDMockito.given(locationRepository.findById(locationUpdateDTO.getParentLocationId())).willReturn(Optional.of(location8));

        Assertions.assertThrows(IllegalLocationParentException.class,
                () -> locationService.update(location8.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location8.getId());
    }

    @Test
    void updateChildOfChildLocation() {
        LocationUpdateDTO locationUpdateDTO = toUpdateDTO(location1);
        locationUpdateDTO.setParentLocationId(location4.getId());

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.of(location1));
        BDDMockito.given(locationRepository.findById(locationUpdateDTO.getParentLocationId())).willReturn(Optional.of(location4));
        BDDMockito.given(locationRepository.findAllByParentLocation(location1)).willReturn(List.of(location3));
        BDDMockito.given(locationRepository.findAllByParentLocation(location3)).willReturn(Arrays.asList(location4, location6));
        BDDMockito.given(locationRepository.findAllByParentLocation(location4)).willReturn(List.of(location5));
        BDDMockito.given(locationRepository.findAllByParentLocation(location6)).willReturn(new ArrayList<>());
        BDDMockito.given(locationRepository.findAllByParentLocation(location5)).willReturn(List.of(location7));
        BDDMockito.given(locationRepository.findAllByParentLocation(location7)).willReturn(new ArrayList<>());
        Assertions.assertThrows(IllegalLocationParentException.class,
                () -> locationService.update(location1.getId(), locationUpdateDTO, new Image()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location1.getId());
    }

    @Test
    void deleteLocation()  throws NotFoundByIdException {
        BDDMockito.given(locationRepository.findById(location8.getId())).willReturn(Optional.of(location8));

        locationService.deleteById(location8.getId());

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location8.getId());
    }

    @Test
    void deleteNonexistentLocation() {
        BDDMockito.given(locationRepository.findById(location8.getId())).willReturn(Optional.empty());


        Assertions.assertThrows(NotFoundByIdException.class,
                ()-> locationService.deleteById(location8.getId()));

        Mockito.verify(locationRepository, Mockito.atLeastOnce()).findById(location8.getId());
    }

    @Test
    void getAllChildLocations() {
        LocationCreateDTO locationCreateDTO = toCreateDTO(location1);
        locationCreateDTO.setParentLocationId(location4.getId());

        BDDMockito.given(locationRepository.findById(location1.getId())).willReturn(Optional.of(location1));
        BDDMockito.given(locationRepository.findById(locationCreateDTO.getParentLocationId())).willReturn(Optional.of(location4));
        BDDMockito.given(locationRepository.findAllByParentLocation(location1)).willReturn(List.of(location3));
        BDDMockito.given(locationRepository.findAllByParentLocation(location3)).willReturn(Arrays.asList(location4, location6));
        BDDMockito.given(locationRepository.findAllByParentLocation(location4)).willReturn(List.of(location5));
        BDDMockito.given(locationRepository.findAllByParentLocation(location6)).willReturn(new ArrayList<>());
        BDDMockito.given(locationRepository.findAllByParentLocation(location5)).willReturn(List.of(location7));
        BDDMockito.given(locationRepository.findAllByParentLocation(location7)).willReturn(new ArrayList<>());
        List<Location> locationList = locationService.getAllChildLocations(location1);
        Assertions.assertEquals(locationList, List.of(location3, location4, location6,
                location5, location7));

    }

    @Test
    void getDirectChildLocations() {
        LocationCreateDTO locationCreateDTO = toCreateDTO(location1);
        locationCreateDTO.setParentLocationId(location4.getId());

        BDDMockito.given(locationRepository.findAllByParentLocation(location3)).willReturn(Arrays.asList(location4, location6));
        List<Location> locationList = locationService.getAllChildLocations(location3);
        Assertions.assertEquals(locationList, List.of(location4, location6));

    }

    @Test
    void getAllParentChildLocations() {

        BDDMockito.given(locationRepository.findAllByParentLocation(location3)).willReturn(Arrays.asList(location4, location6));
        List<Location> locationList = locationService.getLocationsAbove(location6);
        Assertions.assertEquals(locationList, List.of(location6, location3, location1));

    }

    private LocationCreateDTO toCreateDTO(Location location){
        Integer parentId = null;
        if(location.getParentLocation() != null){
            parentId = location.getParentLocation().getId();
        }
        return new LocationCreateDTO(parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                multipartFile);
    }

    private LocationUpdateDTO toUpdateDTO(Location location) {
        Integer parentId = null;
        if(location.getParentLocation() != null){
            parentId = location.getParentLocation().getId();
        }
        return new LocationUpdateDTO(parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                multipartFile);
    }

    private LocationDTO toDTO(Location location) {
        Integer parentId = null;
        if(location.getParentLocation() != null){
            parentId = location.getParentLocation().getId();
        }
        return new LocationDTO(location.getId(),
                parentId,
                location.getName(),
                location.getDescription(),
                location.getSlug(),
                location.getImage().getId());
    }
}
