package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.RoleDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.LocationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = RoleFacade.class)
public class RoleFacadeTest {

    @Autowired
    private RoleFacade roleFacade;

    @MockBean
    private RoleService roleService;

    @MockBean
    private ItemService itemService;

    @MockBean
    private LocationService locationService;

    @MockBean
    private UserService userService;

    @MockBean
    private TagService tagService;

    @MockBean
    private SecurityManager securityManager;

    @MockBean
    private Authentication auth;

    @MockBean
    private SecurityContext context;

    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Location location2 = new Location(null, "locationName2", "locationDescr2", "locationSlug2", new Image());
    Location location3 = new Location(location1, "locationName3", "locationDescr3", "locationSlug3", new Image());
    Location location4 = new Location(location3, "locationName4", "locationDescr4", "locationSlug4", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item2 = new Item("desc", "name", location2, "slug", new ArrayList<>(), new Image(), true);
    Item item3 = new Item("desc", "name", location3, "slug", new ArrayList<>(), new Image(), true);
    Item item4 = new Item("desc", "name", location4, "slug", new ArrayList<>(), new Image(), true);
    Role role1 = new Role(List.of(item1, item2), new ArrayList<>(), List.of(location1), new ArrayList<>(), "a", true);
    Role role2 = new Role(List.of(item1, item3), new ArrayList<>(), List.of(location3), new ArrayList<>(), "a", true);
    Role role3 = new Role(List.of(item4), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    Role role4 = new Role(List.of(item1, item2, item3, item4), List.of(role1, role2, role3),
            List.of(location1, location2, location3, location4), new ArrayList<>(), "a", true);

    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<Role> roles = Arrays.asList(role1, role2, role3, role4);
        List<RoleDTO> rolesDTO = roles.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllRolesAccess(auth)).willReturn(true);
        BDDMockito.given(roleService.findAll()).willReturn(roles);
        Assertions.assertEquals(rolesDTO, roleFacade.findAll());
        Mockito.verify(roleService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException {
        List<Role> roles = Arrays.asList(role1, role2);
        List<Integer> roleIds = roles.stream().map(Role::getId).toList();

        for (Integer id : roleIds) {
            BDDMockito.given(securityManager.getRoleAccess(id, auth)).willReturn(true);
        }

        BDDMockito.given(roleService.findByIds(roleIds)).willReturn(roles);
        Assertions.assertEquals(roles.stream().map(this::toDTO).collect(Collectors.toList()), roleFacade.findByIds(roleIds));
        Mockito.verify(roleService, Mockito.atLeastOnce()).findByIds(roleIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(roleService.findById(role1.getId())).willReturn(Optional.of(role1));
        BDDMockito.given(securityManager.getRoleAccess(role1.getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(role1), roleFacade.findById(role1.getId()));

        Mockito.verify(roleService, Mockito.atLeastOnce()).findById(role1.getId());
    }


    private RoleDTO toDTO(Role role) {
        return new RoleDTO(role.getId(),
                role.getName(),
                role.getManagedItems().stream().map(Item::getId).toList(),
                role.getManagedRoles().stream().map(Role::getId).toList(),
                role.getManagedLocations().stream().map(Location::getId).toList(),
                role.getManagedTags().stream().map(Tag::getId).toList(),
                role.isAdmin());
    }
}
