package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.entity.Ban;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.repository.BanRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = BanService.class)
public class BanServiceTest {

    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, new ArrayList<>(), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, new ArrayList<>(), true, new ArrayList<>());
    Ban ban1 = new Ban(user1, LocalDateTime.now().plusDays(7));
    Ban ban2 = new Ban(user1, LocalDateTime.now().plusDays(777));
    Ban ban3 = new Ban(user2, LocalDateTime.now().plusDays(7));
    Ban ban4 = new Ban(user2, LocalDateTime.now().plusDays(777));
    @Autowired
    private BanService banService;
    @MockBean
    private BanRepository banRepository;

    @Test
    void findAll() {
        List<Ban> bans = Arrays.asList(ban1, ban2, ban3, ban4);

        BDDMockito.given(banRepository.findAll()).willReturn(bans);
        Assertions.assertEquals(bans, banService.findAll());
        Mockito.verify(banRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() {
        List<Ban> bans = Arrays.asList(ban1, ban2);
        List<Integer> bansIds = bans.stream().map(Ban::getId).toList();

        BDDMockito.given(banRepository.findAllById(bansIds)).willReturn(bans);
        Assertions.assertEquals(bans, banService.findByIds(bansIds));
        Mockito.verify(banRepository, Mockito.atLeastOnce()).findAllById(bansIds);
    }

    @Test
    void findById() {

        BDDMockito.given(banRepository.findById(ban1.getId())).willReturn(Optional.of(ban1));

        Assertions.assertEquals(Optional.of(ban1), banService.findById(ban1.getId()));

        Mockito.verify(banRepository, Mockito.atLeastOnce()).findById(ban1.getId());
    }

    @Test
    void findByUser() {

        BDDMockito.given(banRepository.findAllByUser(user1)).willReturn(List.of(ban1, ban2));

        Assertions.assertEquals(List.of(ban1, ban2), banService.findByUser(user1));

        Mockito.verify(banRepository, Mockito.atLeastOnce()).findAllByUser(user1);
    }
}