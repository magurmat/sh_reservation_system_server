package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.TagDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.TagService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = TagFacade.class)
public class TagFacadeTest {

    @Autowired
    TagFacade tagFacade;
    Tag tag1 = new Tag("tagName1");
    Tag tag2 = new Tag("tagName2");
    Tag tag3 = new Tag("tagName3");
    Tag tag4 = new Tag("tagName4");
    @MockBean
    private TagService tagService;
    @MockBean
    private ItemService itemService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private SecurityManager securityManager;
    @MockBean
    private Authentication auth;
    @MockBean
    private SecurityContext context;

    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<Tag> tags = Arrays.asList(tag1, tag2, tag3, tag4);
        List<TagDTO> tagsDTO = tags.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllTagsAccess(auth)).willReturn(true);
        BDDMockito.given(tagService.findAll()).willReturn(tags);
        Assertions.assertEquals(tagsDTO, tagFacade.findAll());
        Mockito.verify(tagService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException {
        List<Tag> tags = Arrays.asList(tag1, tag2);
        List<Integer> tagIds = tags.stream().map(Tag::getId).toList();

        for (Integer id : tagIds) {
            BDDMockito.given(securityManager.getTagAccess(id, auth)).willReturn(true);
        }

        BDDMockito.given(tagService.findByIds(tagIds)).willReturn(tags);
        Assertions.assertEquals(tags.stream().map(this::toDTO).collect(Collectors.toList()), tagFacade.findByIds(tagIds));
        Mockito.verify(tagService, Mockito.atLeastOnce()).findByIds(tagIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(tagService.findById(tag1.getId())).willReturn(Optional.of(tag1));
        BDDMockito.given(securityManager.getTagAccess(tag1.getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(tag1), tagFacade.findById(tag1.getId()));

        Mockito.verify(tagService, Mockito.atLeastOnce()).findById(tag1.getId());
    }

    private TagDTO toDTO(Tag tag) {
        return new TagDTO(tag.getId(),
                tag.getName());
    }

}
