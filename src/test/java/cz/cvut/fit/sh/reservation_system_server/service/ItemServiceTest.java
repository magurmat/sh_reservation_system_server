package cz.cvut.fit.sh.reservation_system_server.service;

import cz.cvut.fit.sh.reservation_system_server.dto.ItemCreateDTO;
import cz.cvut.fit.sh.reservation_system_server.dto.ItemDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Tag;
import cz.cvut.fit.sh.reservation_system_server.repository.ItemRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = ItemService.class)
public class ItemServiceTest {

    @Autowired
    private ItemService itemService;

    @MockBean
    private ItemRepository itemRepository;

    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Tag tag = new Tag("tag");
    Item item1 = new Item("desc", "name", location1, "slug1", List.of(tag), new Image(), true);
    Item item2 = new Item("desc", "name", location1, "slug2", new ArrayList<>(), new Image(), true);
    Item item3 = new Item("desc", "name", location1, "slug3", new ArrayList<>(), new Image(), true);
    Item item4 = new Item("desc", "name", location1, "slug4", List.of(tag), new Image(), true);

    @Test
    void findAll() {
        List<Item> items = Arrays.asList(item1, item2, item3, item4);

        BDDMockito.given(itemRepository.findAll()).willReturn(items);
        Assertions.assertEquals(items, itemService.findAll());
        Mockito.verify(itemRepository, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds(){
        List<Item> items= Arrays.asList(item1, item2);
        List<Integer> itemsIds= items.stream().map(Item::getId).toList();

        BDDMockito.given(itemRepository.findAllById(itemsIds)).willReturn(items);
        Assertions.assertEquals(items, itemService.findByIds(itemsIds));
        Mockito.verify(itemRepository,Mockito.atLeastOnce()).findAllById(itemsIds);
    }

    @Test
    void findById() {

        BDDMockito.given(itemRepository.findById(item1.getId())).willReturn(Optional.of(item1));

        Assertions.assertEquals(Optional.of(item1), itemService.findById(item1.getId()));

        Mockito.verify(itemRepository, Mockito.atLeastOnce()).findById(item1.getId());
    }

    @Test
    void findBySlug() {

        BDDMockito.given(itemRepository.findBySlug(item1.getSlug())).willReturn(Optional.of(item1));

        Assertions.assertEquals(Optional.of(item1), itemService.findBySlug(item1.getSlug()));

        Mockito.verify(itemRepository, Mockito.atLeastOnce()).findBySlug(item1.getSlug());
    }

    @Test
    void findByTag() {

        BDDMockito.given(itemRepository.findAllByTagsIn(List.of(tag))).willReturn(List.of(item1, item4));

        Assertions.assertEquals(List.of(item1, item4), itemService.findByTag(tag));

        Mockito.verify(itemRepository, Mockito.atLeastOnce()).findAllByTagsIn(List.of(tag));
    }

    @Test
    void findByLocation() {

        BDDMockito.given(itemRepository.findAllByLocation(location1)).willReturn(List.of(item1, item2, item3, item4));

        Assertions.assertEquals(List.of(item1, item2, item3, item4), itemService.findByLocation(location1));

        Mockito.verify(itemRepository, Mockito.atLeastOnce()).findAllByLocation(location1);
    }


    @Test
    void create() {
        ItemCreateDTO itemCreateDTO = new ItemCreateDTO(item1.getDescription(), item1.getName(), location1.getId(),
                "slug1", new ArrayList<>(), null, true);
        ItemDTO itemDTO = toDTO(item1);


        BDDMockito.given(itemRepository.save(new Item(
                itemCreateDTO.getDescription(),
                itemCreateDTO.getName(),
                location1,
                itemCreateDTO.getSlug(),
                new ArrayList<>(),
                new Image(),
                itemCreateDTO.getEnabled()
        ))).willReturn(new Item(
                itemCreateDTO.getDescription(),
                itemCreateDTO.getName(),
                location1,
                itemCreateDTO.getSlug(),
                new ArrayList<>(),
                new Image(),
                itemCreateDTO.getEnabled()
        ));
        Assertions.assertEquals(new Item(
                itemCreateDTO.getDescription(),
                itemCreateDTO.getName(),
                location1,
                itemCreateDTO.getSlug(),
                new ArrayList<>(),
                new Image(),
                itemCreateDTO.getEnabled()
        ), itemService.create(itemCreateDTO, location1, new ArrayList<>(), new Image()));

        Mockito.verify(itemRepository, Mockito.atLeastOnce()).save(new Item(
                itemCreateDTO.getDescription(),
                itemCreateDTO.getName(),
                location1,
                itemCreateDTO.getSlug(),
                new ArrayList<>(),
                new Image(),
                itemCreateDTO.getEnabled()
        ));

    }

    private ItemDTO toDTO(Item item) {
        return new ItemDTO(item.getId(), item.getDescription(), item.getName(),item.getLocation().getId(), item.getSlug(),
                item.getTags().stream().map(Tag::getId).toList(), item.getImage().getId(), item.getEnabled());
    }
}
