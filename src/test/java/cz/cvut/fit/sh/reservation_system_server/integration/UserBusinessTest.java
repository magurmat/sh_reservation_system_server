package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.UserDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.facade.UserFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.UserRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.BanService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RefreshTokenService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {UserFacade.class, UserService.class})
public class UserBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(UserBusinessTest.class);
    Role role1 = new Role(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    Role role2 = new Role(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, List.of(role1), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, List.of(role1), true, new ArrayList<>());
    User user3 = new User("firstname", "lastname", "email", "password",
            "username3", 3, List.of(role1), true, new ArrayList<>());
    User user4 = new User("firstname", "lastname", "email", "password",
            "username4", 4, List.of(role1, role2), true, new ArrayList<>());
    List<User> users = List.of(user1, user2, user3, user4);
    @Autowired
    private UserService userService;
    @Autowired
    private UserFacade userFacade;
    @MockBean
    private ReservationService reservationService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private ItemService itemService;
    @MockBean
    private BanService banService;
    @MockBean
    private RefreshTokenService refreshTokenService;
    @MockBean
    private SecurityManager securityManager;
    @MockBean
    private UserRepository userRepository;

    @Test
    void getUsers() throws Exception {

        BDDMockito.given(userRepository.findAll()).willReturn(users);
        BDDMockito.given(securityManager.getAllUsersAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (userFacade.findAll().size() == users.size());
    }

    @Test
    void getUser() throws Exception {

        BDDMockito.given(userRepository.findById(user1.getId())).willReturn(Optional.ofNullable(user1));
        BDDMockito.given(securityManager.getUserAccess(user1.getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(userFacade.findById(user1.getId()), toDTO(user1)));
    }

    private UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getPassword(), user.getUsername(), user.getUid(),
                user.getRoles().stream().map(Role::getId).toList(),
                user.getFavourites().stream().map(Item::getId).toList(), user.isActive());
    }
}
