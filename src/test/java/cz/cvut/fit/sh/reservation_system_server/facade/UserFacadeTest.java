package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.UserDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Role;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.BanService;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.RefreshTokenService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = UserFacade.class)
public class UserFacadeTest {

    @Autowired
    private UserFacade userFacade;

    @MockBean
    private UserService userService;

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private ItemService itemService;

    @MockBean
    private BanService banService;

    @MockBean
    private RefreshTokenService refreshTokenService;

    @MockBean
    private SecurityManager securityManager;

    @MockBean
    private Authentication auth;

    @MockBean
    private SecurityContext context;

    Role role1 = new Role(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    Role role2 = new Role(new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), new ArrayList<>(), "a", true);
    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, List.of(role1), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, List.of(role1), true, new ArrayList<>());
    User user3 = new User("firstname", "lastname", "email", "password",
            "username3", 3, List.of(role1), true, new ArrayList<>());
    User user4 = new User("firstname", "lastname", "email", "password",
            "username4", 4, List.of(role1, role2), true, new ArrayList<>());

    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<User> users = Arrays.asList(user1, user2, user3, user4);
        List<UserDTO> usersDTO = users.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllUsersAccess(auth)).willReturn(true);
        BDDMockito.given(userService.findAll()).willReturn(users);
        Assertions.assertEquals(usersDTO, userFacade.findAll());
        Mockito.verify(userService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException {
        List<User> users = Arrays.asList(user1, user2);
        List<Integer> userIds = users.stream().map(User::getId).toList();

        for (Integer id : userIds) {
            BDDMockito.given(securityManager.getUserAccess(id, auth)).willReturn(true);
        }

        BDDMockito.given(userService.findByIds(userIds)).willReturn(users);
        Assertions.assertEquals(users.stream().map(this::toDTO).collect(Collectors.toList()), userFacade.findByIds(userIds));
        Mockito.verify(userService, Mockito.atLeastOnce()).findByIds(userIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(userService.findById(user1.getId())).willReturn(Optional.of(user1));
        BDDMockito.given(securityManager.getUserAccess(user1.getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(user1), userFacade.findById(user1.getId()));

        Mockito.verify(userService, Mockito.atLeastOnce()).findById(user1.getId());
    }

    private UserDTO toDTO(User user) {
        return new UserDTO(user.getId(), user.getFirstName(), user.getLastName(),
                user.getEmail(), user.getPassword(), user.getUsername(), user.getUid(),
                user.getRoles().stream().map(Role::getId).toList(),
                user.getFavourites().stream().map(Item::getId).toList(),user.isActive());
    }
}
