package cz.cvut.fit.sh.reservation_system_server.facade;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.exception.NotFoundByIdException;
import cz.cvut.fit.sh.reservation_system_server.exception.UnauthorizedException;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@SpringBootTest(classes = ReservationFacade.class)
public class ReservationFacadeTest {

    @Autowired
    private ReservationFacade reservationFacade;

    @MockBean
    private ReservationService reservationService;

    @MockBean
    private ItemService itemService;

    @MockBean
    private UserService userService;

    @MockBean
    private RoleService roleService;

    @MockBean
    private SecurityManager securityManager;

    @MockBean
    private Authentication auth;

    @MockBean
    private SecurityContext context;

    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, new ArrayList<>(), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, new ArrayList<>(), true, new ArrayList<>());
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Item item2 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Reservation reservation1 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item1);
    Reservation reservation2 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item1);
    Reservation reservation3 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item2);
    Reservation reservation4 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item2);


    @BeforeEach
    void init() {
        SecurityContextHolder.setContext(context);
        BDDMockito.given(context.getAuthentication()).willReturn(auth);
    }

    @Test
    void findAll() throws UnauthorizedException {
        List<Reservation> reservations = Arrays.asList(reservation1, reservation2, reservation3, reservation4);
        List<ReservationDTO> reservationsDTO = reservations.stream().map(this::toDTO).toList();

        BDDMockito.given(securityManager.getAllReservationsAccess(auth)).willReturn(true);
        BDDMockito.given(reservationService.findAll()).willReturn(reservations);
        Assertions.assertEquals(reservationsDTO, reservationFacade.findAll());
        Mockito.verify(reservationService, Mockito.atLeastOnce()).findAll();
    }

    @Test
    void findByIds() throws UnauthorizedException, NotFoundByIdException {
        List<Reservation> reservations = Arrays.asList(reservation1, reservation2);
        List<Integer> reservationIds = reservations.stream().map(Reservation::getId).toList();

        for (Reservation res : reservations) {
            BDDMockito.given(securityManager.getReservationAccess(res.getUser().getId(), res.getItem().getId(),
                    res.getItem().getLocation().getId(), auth)).willReturn(true);
            BDDMockito.given(reservationService.findById(res.getId())).willReturn(Optional.of(res));
        }

        BDDMockito.given(reservationService.findByIds(reservationIds)).willReturn(reservations);
        Assertions.assertEquals(reservations.stream().map(this::toDTO).collect(Collectors.toList()), reservationFacade.findByIds(reservationIds));
        Mockito.verify(reservationService, Mockito.atLeastOnce()).findByIds(reservationIds);
    }

    @Test
    void findById() throws NotFoundByIdException, UnauthorizedException {

        BDDMockito.given(reservationService.findById(reservation1.getId())).willReturn(Optional.of(reservation1));
        BDDMockito.given(securityManager.getReservationAccess(reservation1.getUser().getId(), reservation1.getItem().getId(),
                reservation1.getItem().getLocation().getId(), auth)).willReturn(true);
        Assertions.assertEquals(toDTO(reservation1), reservationFacade.findById(reservation1.getId()));

        Mockito.verify(reservationService, Mockito.atLeastOnce()).findById(reservation1.getId());
    }


    private ReservationDTO toDTO(Reservation reservation) {
        return new ReservationDTO(reservation.getId(),
                reservation.getStartTime(),
                reservation.getEndTime(),
                reservation.getState(),
                reservation.getUser().getId(),
                reservation.getUser().getFirstName(),
                reservation.getUser().getLastName(),
                reservation.getItem().getId());
    }
}
