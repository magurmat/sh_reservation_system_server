package cz.cvut.fit.sh.reservation_system_server.integration;

import cz.cvut.fit.sh.reservation_system_server.dto.ReservationDTO;
import cz.cvut.fit.sh.reservation_system_server.entity.Image;
import cz.cvut.fit.sh.reservation_system_server.entity.Item;
import cz.cvut.fit.sh.reservation_system_server.entity.Location;
import cz.cvut.fit.sh.reservation_system_server.entity.Reservation;
import cz.cvut.fit.sh.reservation_system_server.entity.ReservationState;
import cz.cvut.fit.sh.reservation_system_server.entity.User;
import cz.cvut.fit.sh.reservation_system_server.facade.ReservationFacade;
import cz.cvut.fit.sh.reservation_system_server.repository.ReservationRepository;
import cz.cvut.fit.sh.reservation_system_server.security.SecurityManager;
import cz.cvut.fit.sh.reservation_system_server.service.ItemService;
import cz.cvut.fit.sh.reservation_system_server.service.ReservationService;
import cz.cvut.fit.sh.reservation_system_server.service.RoleService;
import cz.cvut.fit.sh.reservation_system_server.service.UserService;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.context.SecurityContextHolder;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@SpringBootTest(classes = {ReservationFacade.class, ReservationService.class})
public class ReservationBusinessTest {

    private static final Logger logger = LoggerFactory.getLogger(ReservationBusinessTest.class);
    User user1 = new User("firstname", "lastname", "email", "password",
            "username1", 1, new ArrayList<>(), true, new ArrayList<>());
    User user2 = new User("firstname", "lastname", "email", "password",
            "username2", 2, new ArrayList<>(), true, new ArrayList<>());
    Location location1 = new Location(null, "locationName1", "locationDescr1", "locationSlug1", new Image());
    Item item1 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Reservation reservation1 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item1);
    Reservation reservation2 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item1);
    Item item2 = new Item("desc", "name", location1, "slug", new ArrayList<>(), new Image(), true);
    Reservation reservation3 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user1, item2);
    Reservation reservation4 = new Reservation(LocalDateTime.now(), LocalDateTime.now().plusDays(1),
            ReservationState.CREATED, user2, item2);
    List<Reservation> reservations = List.of(reservation1, reservation2, reservation3, reservation4);
    @Autowired
    private ReservationService reservationService;
    @Autowired
    private ReservationFacade reservationFacade;
    @MockBean
    private ReservationRepository reservationRepository;
    @MockBean
    private ItemService itemService;
    @MockBean
    private UserService userService;
    @MockBean
    private RoleService roleService;
    @MockBean
    private SecurityManager securityManager;

    @Test
    void getReservations() throws Exception {

        BDDMockito.given(reservationRepository.findAll()).willReturn(reservations);
        BDDMockito.given(securityManager.getAllReservationsAccess(SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (reservationFacade.findAll().size() == reservations.size());
    }

    @Test
    void getReservation() throws Exception {

        BDDMockito.given(reservationRepository.findById(reservation1.getId())).willReturn(Optional.ofNullable(reservation1));
        BDDMockito.given(securityManager.getReservationAccess(reservation1.getUser().getId(), reservation1.getItem().getId(),
                item1.getLocation().getId(), SecurityContextHolder.getContext().getAuthentication())).willReturn(true);

        assert (Objects.equals(reservationFacade.findById(reservation1.getId()), toDTO(reservation1)));
    }

    private ReservationDTO toDTO(Reservation reservation) {
        return new ReservationDTO(reservation.getId(),
                reservation.getStartTime(),
                reservation.getEndTime(),
                reservation.getState(),
                reservation.getUser().getId(),
                reservation.getUser().getFirstName(),
                reservation.getUser().getLastName(),
                reservation.getItem().getId());
    }
}
