FROM maven:3.8.3-openjdk-17

COPY ./src ./app/src
COPY pom.xml /app/pom.xml
RUN mvn -B -f /app/pom.xml -s /usr/share/maven/ref/settings-docker.xml clean package spring-boot:repackage
ENTRYPOINT ["java","-jar","/app/target/app.jar"]
