## Installation

First, download this repository

Then make sure you have docker installed. After that go to the root directory of this project and run

`docker volume create --name=postgres_data`

`docker-compose build`

Now everytime you want to start this project you will need to run

`docker-compose up`

from the root directory of this project.

## Documentation

Run project and docs will be available [Swagger UI page](http://localhost:8000/api/v1/swagger-ui/index.html)
and [OpenAPI description](http://localhost:8000/api/v1/api-docs)
